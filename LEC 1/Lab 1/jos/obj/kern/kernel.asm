
obj/kern/kernel:     file format elf32-i386


Disassembly of section .text:

f0100000 <_start+0xeffffff4>:
.globl		_start
_start = RELOC(entry)

.globl entry
entry:
	movw	$0x1234,0x472			# warm boot
f0100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
f0100006:	00 00                	add    %al,(%eax)
f0100008:	fe 4f 52             	decb   0x52(%edi)
f010000b:	e4                   	.byte 0xe4

f010000c <entry>:
f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
f0100013:	34 12 
	# sufficient until we set up our real page table in mem_init
	# in lab 2.

	# Load the physical address of entry_pgdir into cr3.  entry_pgdir
	# is defined in entrypgdir.c.
	movl	$(RELOC(entry_pgdir)), %eax
f0100015:	b8 00 00 11 00       	mov    $0x110000,%eax
	movl	%eax, %cr3
f010001a:	0f 22 d8             	mov    %eax,%cr3
	# Turn on paging.
	movl	%cr0, %eax
f010001d:	0f 20 c0             	mov    %cr0,%eax
	orl	$(CR0_PE|CR0_PG|CR0_WP), %eax
f0100020:	0d 01 00 01 80       	or     $0x80010001,%eax
	movl	%eax, %cr0
f0100025:	0f 22 c0             	mov    %eax,%cr0

	# Now paging is enabled, but we're still running at a low EIP
	# (why is this okay?).  Jump up above KERNBASE before entering
	# C code.
	mov	$relocated, %eax
f0100028:	b8 2f 00 10 f0       	mov    $0xf010002f,%eax
	jmp	*%eax
f010002d:	ff e0                	jmp    *%eax

f010002f <relocated>:
relocated:

	# Clear the frame pointer register (EBP)
	# so that once we get into debugging C code,
	# stack backtraces will be terminated properly.
	movl	$0x0,%ebp			# nuke frame pointer
f010002f:	bd 00 00 00 00       	mov    $0x0,%ebp

	# Set the stack pointer
	movl	$(bootstacktop),%esp
f0100034:	bc 00 00 11 f0       	mov    $0xf0110000,%esp

	# now to C code
	call	i386_init
f0100039:	e8 56 00 00 00       	call   f0100094 <i386_init>

f010003e <spin>:

	# Should never get here, but in case we do, just spin.
spin:	jmp	spin
f010003e:	eb fe                	jmp    f010003e <spin>

f0100040 <test_backtrace>:
#include <kern/console.h>

// Test the stack backtrace function (lab 1 only)
void
test_backtrace(int x)
{
f0100040:	55                   	push   %ebp
f0100041:	89 e5                	mov    %esp,%ebp
f0100043:	53                   	push   %ebx
f0100044:	83 ec 0c             	sub    $0xc,%esp
f0100047:	8b 5d 08             	mov    0x8(%ebp),%ebx
	cprintf("entering test_backtrace %d\n", x);
f010004a:	53                   	push   %ebx
f010004b:	68 e0 18 10 f0       	push   $0xf01018e0
f0100050:	e8 2d 09 00 00       	call   f0100982 <cprintf>
	if (x > 0)
f0100055:	83 c4 10             	add    $0x10,%esp
f0100058:	85 db                	test   %ebx,%ebx
f010005a:	7e 11                	jle    f010006d <test_backtrace+0x2d>
		test_backtrace(x-1);
f010005c:	83 ec 0c             	sub    $0xc,%esp
f010005f:	8d 43 ff             	lea    -0x1(%ebx),%eax
f0100062:	50                   	push   %eax
f0100063:	e8 d8 ff ff ff       	call   f0100040 <test_backtrace>
f0100068:	83 c4 10             	add    $0x10,%esp
f010006b:	eb 11                	jmp    f010007e <test_backtrace+0x3e>
	else
		mon_backtrace(0, 0, 0);
f010006d:	83 ec 04             	sub    $0x4,%esp
f0100070:	6a 00                	push   $0x0
f0100072:	6a 00                	push   $0x0
f0100074:	6a 00                	push   $0x0
f0100076:	e8 d3 06 00 00       	call   f010074e <mon_backtrace>
f010007b:	83 c4 10             	add    $0x10,%esp
	cprintf("leaving test_backtrace %d\n", x);
f010007e:	83 ec 08             	sub    $0x8,%esp
f0100081:	53                   	push   %ebx
f0100082:	68 fc 18 10 f0       	push   $0xf01018fc
f0100087:	e8 f6 08 00 00       	call   f0100982 <cprintf>
}
f010008c:	83 c4 10             	add    $0x10,%esp
f010008f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100092:	c9                   	leave  
f0100093:	c3                   	ret    

f0100094 <i386_init>:

void
i386_init(void)
{
f0100094:	55                   	push   %ebp
f0100095:	89 e5                	mov    %esp,%ebp
f0100097:	83 ec 0c             	sub    $0xc,%esp
	extern char edata[], end[];

	// Before doing anything else, complete the ELF loading process.
	// Clear the uninitialized global data (BSS) section of our program.
	// This ensures that all static/global variables start out zero.
	memset(edata, 0, end - edata);
f010009a:	b8 44 29 11 f0       	mov    $0xf0112944,%eax
f010009f:	2d 00 23 11 f0       	sub    $0xf0112300,%eax
f01000a4:	50                   	push   %eax
f01000a5:	6a 00                	push   $0x0
f01000a7:	68 00 23 11 f0       	push   $0xf0112300
f01000ac:	e8 87 13 00 00       	call   f0101438 <memset>

	// Initialize the console.
	// Can't call cprintf until after we do this!
	cons_init();
f01000b1:	e8 76 04 00 00       	call   f010052c <cons_init>

	cprintf("6828 decimal is %o octal!\n", 6828);
f01000b6:	83 c4 08             	add    $0x8,%esp
f01000b9:	68 ac 1a 00 00       	push   $0x1aac
f01000be:	68 17 19 10 f0       	push   $0xf0101917
f01000c3:	e8 ba 08 00 00       	call   f0100982 <cprintf>

	// Test the stack backtrace function (lab 1 only)
	test_backtrace(5);
f01000c8:	c7 04 24 05 00 00 00 	movl   $0x5,(%esp)
f01000cf:	e8 6c ff ff ff       	call   f0100040 <test_backtrace>
f01000d4:	83 c4 10             	add    $0x10,%esp

	// Drop into the kernel monitor.
	while (1)
		monitor(NULL);
f01000d7:	83 ec 0c             	sub    $0xc,%esp
f01000da:	6a 00                	push   $0x0
f01000dc:	e8 34 07 00 00       	call   f0100815 <monitor>
f01000e1:	83 c4 10             	add    $0x10,%esp
f01000e4:	eb f1                	jmp    f01000d7 <i386_init+0x43>

f01000e6 <_panic>:
 * Panic is called on unresolvable fatal errors.
 * It prints "panic: mesg", and then enters the kernel monitor.
 */
void
_panic(const char *file, int line, const char *fmt,...)
{
f01000e6:	55                   	push   %ebp
f01000e7:	89 e5                	mov    %esp,%ebp
f01000e9:	56                   	push   %esi
f01000ea:	53                   	push   %ebx
f01000eb:	8b 75 10             	mov    0x10(%ebp),%esi
	va_list ap;

	if (panicstr)
f01000ee:	83 3d 40 29 11 f0 00 	cmpl   $0x0,0xf0112940
f01000f5:	75 37                	jne    f010012e <_panic+0x48>
		goto dead;
	panicstr = fmt;
f01000f7:	89 35 40 29 11 f0    	mov    %esi,0xf0112940

	// Be extra sure that the machine is in as reasonable state
	__asm __volatile("cli; cld");
f01000fd:	fa                   	cli    
f01000fe:	fc                   	cld    

	va_start(ap, fmt);
f01000ff:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel panic at %s:%d: ", file, line);
f0100102:	83 ec 04             	sub    $0x4,%esp
f0100105:	ff 75 0c             	pushl  0xc(%ebp)
f0100108:	ff 75 08             	pushl  0x8(%ebp)
f010010b:	68 32 19 10 f0       	push   $0xf0101932
f0100110:	e8 6d 08 00 00       	call   f0100982 <cprintf>
	vcprintf(fmt, ap);
f0100115:	83 c4 08             	add    $0x8,%esp
f0100118:	53                   	push   %ebx
f0100119:	56                   	push   %esi
f010011a:	e8 3d 08 00 00       	call   f010095c <vcprintf>
	cprintf("\n");
f010011f:	c7 04 24 6e 19 10 f0 	movl   $0xf010196e,(%esp)
f0100126:	e8 57 08 00 00       	call   f0100982 <cprintf>
	va_end(ap);
f010012b:	83 c4 10             	add    $0x10,%esp

dead:
	/* break into the kernel monitor */
	while (1)
		monitor(NULL);
f010012e:	83 ec 0c             	sub    $0xc,%esp
f0100131:	6a 00                	push   $0x0
f0100133:	e8 dd 06 00 00       	call   f0100815 <monitor>
f0100138:	83 c4 10             	add    $0x10,%esp
f010013b:	eb f1                	jmp    f010012e <_panic+0x48>

f010013d <_warn>:
}

/* like panic, but don't */
void
_warn(const char *file, int line, const char *fmt,...)
{
f010013d:	55                   	push   %ebp
f010013e:	89 e5                	mov    %esp,%ebp
f0100140:	53                   	push   %ebx
f0100141:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100144:	8d 5d 14             	lea    0x14(%ebp),%ebx
	cprintf("kernel warning at %s:%d: ", file, line);
f0100147:	ff 75 0c             	pushl  0xc(%ebp)
f010014a:	ff 75 08             	pushl  0x8(%ebp)
f010014d:	68 4a 19 10 f0       	push   $0xf010194a
f0100152:	e8 2b 08 00 00       	call   f0100982 <cprintf>
	vcprintf(fmt, ap);
f0100157:	83 c4 08             	add    $0x8,%esp
f010015a:	53                   	push   %ebx
f010015b:	ff 75 10             	pushl  0x10(%ebp)
f010015e:	e8 f9 07 00 00       	call   f010095c <vcprintf>
	cprintf("\n");
f0100163:	c7 04 24 6e 19 10 f0 	movl   $0xf010196e,(%esp)
f010016a:	e8 13 08 00 00       	call   f0100982 <cprintf>
	va_end(ap);
}
f010016f:	83 c4 10             	add    $0x10,%esp
f0100172:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0100175:	c9                   	leave  
f0100176:	c3                   	ret    

f0100177 <serial_proc_data>:

static bool serial_exists;

static int
serial_proc_data(void)
{
f0100177:	55                   	push   %ebp
f0100178:	89 e5                	mov    %esp,%ebp

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010017a:	ba fd 03 00 00       	mov    $0x3fd,%edx
f010017f:	ec                   	in     (%dx),%al
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
f0100180:	a8 01                	test   $0x1,%al
f0100182:	74 0b                	je     f010018f <serial_proc_data+0x18>
f0100184:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100189:	ec                   	in     (%dx),%al
		return -1;
	return inb(COM1+COM_RX);
f010018a:	0f b6 c0             	movzbl %al,%eax
f010018d:	eb 05                	jmp    f0100194 <serial_proc_data+0x1d>

static int
serial_proc_data(void)
{
	if (!(inb(COM1+COM_LSR) & COM_LSR_DATA))
		return -1;
f010018f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
	return inb(COM1+COM_RX);
}
f0100194:	5d                   	pop    %ebp
f0100195:	c3                   	ret    

f0100196 <cons_intr>:

// called by device interrupt routines to feed input characters
// into the circular console input buffer.
static void
cons_intr(int (*proc)(void))
{
f0100196:	55                   	push   %ebp
f0100197:	89 e5                	mov    %esp,%ebp
f0100199:	53                   	push   %ebx
f010019a:	83 ec 04             	sub    $0x4,%esp
f010019d:	89 c3                	mov    %eax,%ebx
	int c;

	while ((c = (*proc)()) != -1) {
f010019f:	eb 2b                	jmp    f01001cc <cons_intr+0x36>
		if (c == 0)
f01001a1:	85 c0                	test   %eax,%eax
f01001a3:	74 27                	je     f01001cc <cons_intr+0x36>
			continue;
		cons.buf[cons.wpos++] = c;
f01001a5:	8b 0d 24 25 11 f0    	mov    0xf0112524,%ecx
f01001ab:	8d 51 01             	lea    0x1(%ecx),%edx
f01001ae:	89 15 24 25 11 f0    	mov    %edx,0xf0112524
f01001b4:	88 81 20 23 11 f0    	mov    %al,-0xfeedce0(%ecx)
		if (cons.wpos == CONSBUFSIZE)
f01001ba:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f01001c0:	75 0a                	jne    f01001cc <cons_intr+0x36>
			cons.wpos = 0;
f01001c2:	c7 05 24 25 11 f0 00 	movl   $0x0,0xf0112524
f01001c9:	00 00 00 
static void
cons_intr(int (*proc)(void))
{
	int c;

	while ((c = (*proc)()) != -1) {
f01001cc:	ff d3                	call   *%ebx
f01001ce:	83 f8 ff             	cmp    $0xffffffff,%eax
f01001d1:	75 ce                	jne    f01001a1 <cons_intr+0xb>
			continue;
		cons.buf[cons.wpos++] = c;
		if (cons.wpos == CONSBUFSIZE)
			cons.wpos = 0;
	}
}
f01001d3:	83 c4 04             	add    $0x4,%esp
f01001d6:	5b                   	pop    %ebx
f01001d7:	5d                   	pop    %ebp
f01001d8:	c3                   	ret    

f01001d9 <kbd_proc_data>:
f01001d9:	ba 64 00 00 00       	mov    $0x64,%edx
f01001de:	ec                   	in     (%dx),%al
{
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
f01001df:	a8 01                	test   $0x1,%al
f01001e1:	0f 84 eb 00 00 00    	je     f01002d2 <kbd_proc_data+0xf9>
f01001e7:	ba 60 00 00 00       	mov    $0x60,%edx
f01001ec:	ec                   	in     (%dx),%al
f01001ed:	89 c2                	mov    %eax,%edx
		return -1;

	data = inb(KBDATAP);

	if (data == 0xE0) {
f01001ef:	3c e0                	cmp    $0xe0,%al
f01001f1:	75 0d                	jne    f0100200 <kbd_proc_data+0x27>
		// E0 escape character
		shift |= E0ESC;
f01001f3:	83 0d 00 23 11 f0 40 	orl    $0x40,0xf0112300
		return 0;
f01001fa:	b8 00 00 00 00       	mov    $0x0,%eax
f01001ff:	c3                   	ret    
	} else if (data & 0x80) {
f0100200:	84 c0                	test   %al,%al
f0100202:	79 2f                	jns    f0100233 <kbd_proc_data+0x5a>
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
f0100204:	8b 0d 00 23 11 f0    	mov    0xf0112300,%ecx
f010020a:	f6 c1 40             	test   $0x40,%cl
f010020d:	75 05                	jne    f0100214 <kbd_proc_data+0x3b>
f010020f:	83 e0 7f             	and    $0x7f,%eax
f0100212:	89 c2                	mov    %eax,%edx
		shift &= ~(shiftcode[data] | E0ESC);
f0100214:	0f b6 c2             	movzbl %dl,%eax
f0100217:	0f b6 80 c0 1a 10 f0 	movzbl -0xfefe540(%eax),%eax
f010021e:	83 c8 40             	or     $0x40,%eax
f0100221:	0f b6 c0             	movzbl %al,%eax
f0100224:	f7 d0                	not    %eax
f0100226:	21 c8                	and    %ecx,%eax
f0100228:	a3 00 23 11 f0       	mov    %eax,0xf0112300
		return 0;
f010022d:	b8 00 00 00 00       	mov    $0x0,%eax
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
}
f0100232:	c3                   	ret    
 * Get data from the keyboard.  If we finish a character, return it.  Else 0.
 * Return -1 if no data.
 */
static int
kbd_proc_data(void)
{
f0100233:	55                   	push   %ebp
f0100234:	89 e5                	mov    %esp,%ebp
f0100236:	53                   	push   %ebx
f0100237:	83 ec 04             	sub    $0x4,%esp
	} else if (data & 0x80) {
		// Key released
		data = (shift & E0ESC ? data : data & 0x7F);
		shift &= ~(shiftcode[data] | E0ESC);
		return 0;
	} else if (shift & E0ESC) {
f010023a:	8b 0d 00 23 11 f0    	mov    0xf0112300,%ecx
f0100240:	f6 c1 40             	test   $0x40,%cl
f0100243:	74 0e                	je     f0100253 <kbd_proc_data+0x7a>
		// Last character was an E0 escape; or with 0x80
		data |= 0x80;
f0100245:	83 c8 80             	or     $0xffffff80,%eax
f0100248:	89 c2                	mov    %eax,%edx
		shift &= ~E0ESC;
f010024a:	83 e1 bf             	and    $0xffffffbf,%ecx
f010024d:	89 0d 00 23 11 f0    	mov    %ecx,0xf0112300
	}

	shift |= shiftcode[data];
f0100253:	0f b6 c2             	movzbl %dl,%eax
	shift ^= togglecode[data];
f0100256:	0f b6 90 c0 1a 10 f0 	movzbl -0xfefe540(%eax),%edx
f010025d:	0b 15 00 23 11 f0    	or     0xf0112300,%edx
f0100263:	0f b6 88 c0 19 10 f0 	movzbl -0xfefe640(%eax),%ecx
f010026a:	31 ca                	xor    %ecx,%edx
f010026c:	89 15 00 23 11 f0    	mov    %edx,0xf0112300

	c = charcode[shift & (CTL | SHIFT)][data];
f0100272:	89 d1                	mov    %edx,%ecx
f0100274:	83 e1 03             	and    $0x3,%ecx
f0100277:	8b 0c 8d a0 19 10 f0 	mov    -0xfefe660(,%ecx,4),%ecx
f010027e:	0f b6 04 01          	movzbl (%ecx,%eax,1),%eax
f0100282:	0f b6 d8             	movzbl %al,%ebx
	if (shift & CAPSLOCK) {
f0100285:	f6 c2 08             	test   $0x8,%dl
f0100288:	74 1a                	je     f01002a4 <kbd_proc_data+0xcb>
		if ('a' <= c && c <= 'z')
f010028a:	89 d8                	mov    %ebx,%eax
f010028c:	8d 4b 9f             	lea    -0x61(%ebx),%ecx
f010028f:	83 f9 19             	cmp    $0x19,%ecx
f0100292:	77 05                	ja     f0100299 <kbd_proc_data+0xc0>
			c += 'A' - 'a';
f0100294:	83 eb 20             	sub    $0x20,%ebx
f0100297:	eb 0b                	jmp    f01002a4 <kbd_proc_data+0xcb>
		else if ('A' <= c && c <= 'Z')
f0100299:	83 e8 41             	sub    $0x41,%eax
f010029c:	83 f8 19             	cmp    $0x19,%eax
f010029f:	77 03                	ja     f01002a4 <kbd_proc_data+0xcb>
			c += 'a' - 'A';
f01002a1:	83 c3 20             	add    $0x20,%ebx
	}

	// Process special keys
	// Ctrl-Alt-Del: reboot
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
f01002a4:	f7 d2                	not    %edx
f01002a6:	f6 c2 06             	test   $0x6,%dl
f01002a9:	75 2d                	jne    f01002d8 <kbd_proc_data+0xff>
f01002ab:	81 fb e9 00 00 00    	cmp    $0xe9,%ebx
f01002b1:	75 25                	jne    f01002d8 <kbd_proc_data+0xff>
		cprintf("Rebooting!\n");
f01002b3:	83 ec 0c             	sub    $0xc,%esp
f01002b6:	68 64 19 10 f0       	push   $0xf0101964
f01002bb:	e8 c2 06 00 00       	call   f0100982 <cprintf>
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01002c0:	ba 92 00 00 00       	mov    $0x92,%edx
f01002c5:	b8 03 00 00 00       	mov    $0x3,%eax
f01002ca:	ee                   	out    %al,(%dx)
f01002cb:	83 c4 10             	add    $0x10,%esp
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002ce:	89 d8                	mov    %ebx,%eax
f01002d0:	eb 08                	jmp    f01002da <kbd_proc_data+0x101>
	int c;
	uint8_t data;
	static uint32_t shift;

	if ((inb(KBSTATP) & KBS_DIB) == 0)
		return -1;
f01002d2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01002d7:	c3                   	ret    
	if (!(~shift & (CTL | ALT)) && c == KEY_DEL) {
		cprintf("Rebooting!\n");
		outb(0x92, 0x3); // courtesy of Chris Frost
	}

	return c;
f01002d8:	89 d8                	mov    %ebx,%eax
}
f01002da:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f01002dd:	c9                   	leave  
f01002de:	c3                   	ret    

f01002df <cons_putc>:
}

// output a character to the console
static void
cons_putc(int c)
{
f01002df:	55                   	push   %ebp
f01002e0:	89 e5                	mov    %esp,%ebp
f01002e2:	57                   	push   %edi
f01002e3:	56                   	push   %esi
f01002e4:	53                   	push   %ebx
f01002e5:	83 ec 0c             	sub    $0xc,%esp
f01002e8:	89 c6                	mov    %eax,%esi
static void
serial_putc(int c)
{
	int i;

	for (i = 0;
f01002ea:	bb 00 00 00 00       	mov    $0x0,%ebx

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f01002ef:	bf fd 03 00 00       	mov    $0x3fd,%edi
f01002f4:	b9 84 00 00 00       	mov    $0x84,%ecx
f01002f9:	eb 09                	jmp    f0100304 <cons_putc+0x25>
f01002fb:	89 ca                	mov    %ecx,%edx
f01002fd:	ec                   	in     (%dx),%al
f01002fe:	ec                   	in     (%dx),%al
f01002ff:	ec                   	in     (%dx),%al
f0100300:	ec                   	in     (%dx),%al
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
	     i++)
f0100301:	83 c3 01             	add    $0x1,%ebx
f0100304:	89 fa                	mov    %edi,%edx
f0100306:	ec                   	in     (%dx),%al
serial_putc(int c)
{
	int i;

	for (i = 0;
	     !(inb(COM1 + COM_LSR) & COM_LSR_TXRDY) && i < 12800;
f0100307:	a8 20                	test   $0x20,%al
f0100309:	75 08                	jne    f0100313 <cons_putc+0x34>
f010030b:	81 fb ff 31 00 00    	cmp    $0x31ff,%ebx
f0100311:	7e e8                	jle    f01002fb <cons_putc+0x1c>
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100313:	ba f8 03 00 00       	mov    $0x3f8,%edx
f0100318:	89 f0                	mov    %esi,%eax
f010031a:	ee                   	out    %al,(%dx)
static void
lpt_putc(int c)
{
	int i;

	for (i = 0; !(inb(0x378+1) & 0x80) && i < 12800; i++)
f010031b:	bb 00 00 00 00       	mov    $0x0,%ebx

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100320:	bf 79 03 00 00       	mov    $0x379,%edi
f0100325:	b9 84 00 00 00       	mov    $0x84,%ecx
f010032a:	eb 09                	jmp    f0100335 <cons_putc+0x56>
f010032c:	89 ca                	mov    %ecx,%edx
f010032e:	ec                   	in     (%dx),%al
f010032f:	ec                   	in     (%dx),%al
f0100330:	ec                   	in     (%dx),%al
f0100331:	ec                   	in     (%dx),%al
f0100332:	83 c3 01             	add    $0x1,%ebx
f0100335:	89 fa                	mov    %edi,%edx
f0100337:	ec                   	in     (%dx),%al
f0100338:	81 fb ff 31 00 00    	cmp    $0x31ff,%ebx
f010033e:	7f 04                	jg     f0100344 <cons_putc+0x65>
f0100340:	84 c0                	test   %al,%al
f0100342:	79 e8                	jns    f010032c <cons_putc+0x4d>
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100344:	ba 78 03 00 00       	mov    $0x378,%edx
f0100349:	89 f0                	mov    %esi,%eax
f010034b:	ee                   	out    %al,(%dx)
f010034c:	ba 7a 03 00 00       	mov    $0x37a,%edx
f0100351:	b8 0d 00 00 00       	mov    $0xd,%eax
f0100356:	ee                   	out    %al,(%dx)
f0100357:	b8 08 00 00 00       	mov    $0x8,%eax
f010035c:	ee                   	out    %al,(%dx)
	uint8_t back_color = (uint8_t)back;

	uint8_t attribute_byte = (fore_color & 0x0F) | (back_color << 4);
	uint16_t attribute = attribute_byte << 8;

	c |= attribute;
f010035d:	89 f0                	mov    %esi,%eax
f010035f:	80 cc 0f             	or     $0xf,%ah
	// if no attribute given, then use black on white
	if (!(c & ~0xFF)){
		c |= 0x0700;
	}

	switch (c & 0xff) {
f0100362:	89 f3                	mov    %esi,%ebx
f0100364:	0f b6 f3             	movzbl %bl,%esi
f0100367:	83 fe 09             	cmp    $0x9,%esi
f010036a:	74 72                	je     f01003de <cons_putc+0xff>
f010036c:	83 fe 09             	cmp    $0x9,%esi
f010036f:	7f 0a                	jg     f010037b <cons_putc+0x9c>
f0100371:	83 fe 08             	cmp    $0x8,%esi
f0100374:	74 14                	je     f010038a <cons_putc+0xab>
f0100376:	e9 97 00 00 00       	jmp    f0100412 <cons_putc+0x133>
f010037b:	83 fe 0a             	cmp    $0xa,%esi
f010037e:	74 38                	je     f01003b8 <cons_putc+0xd9>
f0100380:	83 fe 0d             	cmp    $0xd,%esi
f0100383:	74 3b                	je     f01003c0 <cons_putc+0xe1>
f0100385:	e9 88 00 00 00       	jmp    f0100412 <cons_putc+0x133>
	case '\b':
		if (crt_pos > 0) {
f010038a:	0f b7 15 28 25 11 f0 	movzwl 0xf0112528,%edx
f0100391:	66 85 d2             	test   %dx,%dx
f0100394:	0f 84 e4 00 00 00    	je     f010047e <cons_putc+0x19f>
			crt_pos--;
f010039a:	83 ea 01             	sub    $0x1,%edx
f010039d:	66 89 15 28 25 11 f0 	mov    %dx,0xf0112528
			crt_buf[crt_pos] = (c & ~0xff) | ' ';
f01003a4:	0f b7 d2             	movzwl %dx,%edx
f01003a7:	b0 00                	mov    $0x0,%al
f01003a9:	83 c8 20             	or     $0x20,%eax
f01003ac:	8b 0d 2c 25 11 f0    	mov    0xf011252c,%ecx
f01003b2:	66 89 04 51          	mov    %ax,(%ecx,%edx,2)
f01003b6:	eb 78                	jmp    f0100430 <cons_putc+0x151>
		}
		break;
	case '\n':
		crt_pos += CRT_COLS;
f01003b8:	66 83 05 28 25 11 f0 	addw   $0x50,0xf0112528
f01003bf:	50 
		/* fallthru */
	case '\r':
		crt_pos -= (crt_pos % CRT_COLS);
f01003c0:	0f b7 05 28 25 11 f0 	movzwl 0xf0112528,%eax
f01003c7:	69 c0 cd cc 00 00    	imul   $0xcccd,%eax,%eax
f01003cd:	c1 e8 16             	shr    $0x16,%eax
f01003d0:	8d 04 80             	lea    (%eax,%eax,4),%eax
f01003d3:	c1 e0 04             	shl    $0x4,%eax
f01003d6:	66 a3 28 25 11 f0    	mov    %ax,0xf0112528
f01003dc:	eb 52                	jmp    f0100430 <cons_putc+0x151>
		break;
	case '\t':
		cons_putc(' ');
f01003de:	b8 20 00 00 00       	mov    $0x20,%eax
f01003e3:	e8 f7 fe ff ff       	call   f01002df <cons_putc>
		cons_putc(' ');
f01003e8:	b8 20 00 00 00       	mov    $0x20,%eax
f01003ed:	e8 ed fe ff ff       	call   f01002df <cons_putc>
		cons_putc(' ');
f01003f2:	b8 20 00 00 00       	mov    $0x20,%eax
f01003f7:	e8 e3 fe ff ff       	call   f01002df <cons_putc>
		cons_putc(' ');
f01003fc:	b8 20 00 00 00       	mov    $0x20,%eax
f0100401:	e8 d9 fe ff ff       	call   f01002df <cons_putc>
		cons_putc(' ');
f0100406:	b8 20 00 00 00       	mov    $0x20,%eax
f010040b:	e8 cf fe ff ff       	call   f01002df <cons_putc>
f0100410:	eb 1e                	jmp    f0100430 <cons_putc+0x151>
		break;
	default:
		crt_buf[crt_pos++] = c;		/* write the character */
f0100412:	0f b7 15 28 25 11 f0 	movzwl 0xf0112528,%edx
f0100419:	8d 4a 01             	lea    0x1(%edx),%ecx
f010041c:	66 89 0d 28 25 11 f0 	mov    %cx,0xf0112528
f0100423:	0f b7 d2             	movzwl %dx,%edx
f0100426:	8b 0d 2c 25 11 f0    	mov    0xf011252c,%ecx
f010042c:	66 89 04 51          	mov    %ax,(%ecx,%edx,2)
		break;
	}

	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
f0100430:	66 81 3d 28 25 11 f0 	cmpw   $0x7cf,0xf0112528
f0100437:	cf 07 
f0100439:	76 43                	jbe    f010047e <cons_putc+0x19f>
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
f010043b:	a1 2c 25 11 f0       	mov    0xf011252c,%eax
f0100440:	83 ec 04             	sub    $0x4,%esp
f0100443:	68 00 0f 00 00       	push   $0xf00
f0100448:	8d 90 a0 00 00 00    	lea    0xa0(%eax),%edx
f010044e:	52                   	push   %edx
f010044f:	50                   	push   %eax
f0100450:	e8 30 10 00 00       	call   f0101485 <memmove>
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
			crt_buf[i] = 0x0700 | ' ';
f0100455:	8b 15 2c 25 11 f0    	mov    0xf011252c,%edx
f010045b:	8d 82 00 0f 00 00    	lea    0xf00(%edx),%eax
f0100461:	81 c2 a0 0f 00 00    	add    $0xfa0,%edx
f0100467:	83 c4 10             	add    $0x10,%esp
f010046a:	66 c7 00 20 07       	movw   $0x720,(%eax)
f010046f:	83 c0 02             	add    $0x2,%eax
	// What is the purpose of this?
	if (crt_pos >= CRT_SIZE) {
		int i;

		memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
f0100472:	39 d0                	cmp    %edx,%eax
f0100474:	75 f4                	jne    f010046a <cons_putc+0x18b>
			crt_buf[i] = 0x0700 | ' ';
		crt_pos -= CRT_COLS;
f0100476:	66 83 2d 28 25 11 f0 	subw   $0x50,0xf0112528
f010047d:	50 
	}

	/* move that little blinky thing */
	outb(addr_6845, 14);
f010047e:	8b 0d 30 25 11 f0    	mov    0xf0112530,%ecx
f0100484:	b8 0e 00 00 00       	mov    $0xe,%eax
f0100489:	89 ca                	mov    %ecx,%edx
f010048b:	ee                   	out    %al,(%dx)
	outb(addr_6845 + 1, crt_pos >> 8);
f010048c:	0f b7 1d 28 25 11 f0 	movzwl 0xf0112528,%ebx
f0100493:	8d 71 01             	lea    0x1(%ecx),%esi
f0100496:	89 d8                	mov    %ebx,%eax
f0100498:	66 c1 e8 08          	shr    $0x8,%ax
f010049c:	89 f2                	mov    %esi,%edx
f010049e:	ee                   	out    %al,(%dx)
f010049f:	b8 0f 00 00 00       	mov    $0xf,%eax
f01004a4:	89 ca                	mov    %ecx,%edx
f01004a6:	ee                   	out    %al,(%dx)
f01004a7:	89 d8                	mov    %ebx,%eax
f01004a9:	89 f2                	mov    %esi,%edx
f01004ab:	ee                   	out    %al,(%dx)
cons_putc(int c)
{
	serial_putc(c);
	lpt_putc(c);
	cga_putc(c);
}
f01004ac:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01004af:	5b                   	pop    %ebx
f01004b0:	5e                   	pop    %esi
f01004b1:	5f                   	pop    %edi
f01004b2:	5d                   	pop    %ebp
f01004b3:	c3                   	ret    

f01004b4 <serial_intr>:
}

void
serial_intr(void)
{
	if (serial_exists)
f01004b4:	80 3d 34 25 11 f0 00 	cmpb   $0x0,0xf0112534
f01004bb:	74 11                	je     f01004ce <serial_intr+0x1a>
	return inb(COM1+COM_RX);
}

void
serial_intr(void)
{
f01004bd:	55                   	push   %ebp
f01004be:	89 e5                	mov    %esp,%ebp
f01004c0:	83 ec 08             	sub    $0x8,%esp
	if (serial_exists)
		cons_intr(serial_proc_data);
f01004c3:	b8 77 01 10 f0       	mov    $0xf0100177,%eax
f01004c8:	e8 c9 fc ff ff       	call   f0100196 <cons_intr>
}
f01004cd:	c9                   	leave  
f01004ce:	f3 c3                	repz ret 

f01004d0 <kbd_intr>:
	return c;
}

void
kbd_intr(void)
{
f01004d0:	55                   	push   %ebp
f01004d1:	89 e5                	mov    %esp,%ebp
f01004d3:	83 ec 08             	sub    $0x8,%esp
	cons_intr(kbd_proc_data);
f01004d6:	b8 d9 01 10 f0       	mov    $0xf01001d9,%eax
f01004db:	e8 b6 fc ff ff       	call   f0100196 <cons_intr>
}
f01004e0:	c9                   	leave  
f01004e1:	c3                   	ret    

f01004e2 <cons_getc>:
}

// return the next input character from the console, or 0 if none waiting
int
cons_getc(void)
{
f01004e2:	55                   	push   %ebp
f01004e3:	89 e5                	mov    %esp,%ebp
f01004e5:	83 ec 08             	sub    $0x8,%esp
	int c;

	// poll for any pending input characters,
	// so that this function works even when interrupts are disabled
	// (e.g., when called from the kernel monitor).
	serial_intr();
f01004e8:	e8 c7 ff ff ff       	call   f01004b4 <serial_intr>
	kbd_intr();
f01004ed:	e8 de ff ff ff       	call   f01004d0 <kbd_intr>

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
f01004f2:	a1 20 25 11 f0       	mov    0xf0112520,%eax
f01004f7:	3b 05 24 25 11 f0    	cmp    0xf0112524,%eax
f01004fd:	74 26                	je     f0100525 <cons_getc+0x43>
		c = cons.buf[cons.rpos++];
f01004ff:	8d 50 01             	lea    0x1(%eax),%edx
f0100502:	89 15 20 25 11 f0    	mov    %edx,0xf0112520
f0100508:	0f b6 88 20 23 11 f0 	movzbl -0xfeedce0(%eax),%ecx
		if (cons.rpos == CONSBUFSIZE)
			cons.rpos = 0;
		return c;
f010050f:	89 c8                	mov    %ecx,%eax
	kbd_intr();

	// grab the next character from the input buffer.
	if (cons.rpos != cons.wpos) {
		c = cons.buf[cons.rpos++];
		if (cons.rpos == CONSBUFSIZE)
f0100511:	81 fa 00 02 00 00    	cmp    $0x200,%edx
f0100517:	75 11                	jne    f010052a <cons_getc+0x48>
			cons.rpos = 0;
f0100519:	c7 05 20 25 11 f0 00 	movl   $0x0,0xf0112520
f0100520:	00 00 00 
f0100523:	eb 05                	jmp    f010052a <cons_getc+0x48>
		return c;
	}
	return 0;
f0100525:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010052a:	c9                   	leave  
f010052b:	c3                   	ret    

f010052c <cons_init>:
}

// initialize the console devices
void
cons_init(void)
{
f010052c:	55                   	push   %ebp
f010052d:	89 e5                	mov    %esp,%ebp
f010052f:	57                   	push   %edi
f0100530:	56                   	push   %esi
f0100531:	53                   	push   %ebx
f0100532:	83 ec 0c             	sub    $0xc,%esp
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
f0100535:	0f b7 15 00 80 0b f0 	movzwl 0xf00b8000,%edx
	*cp = (uint16_t) 0xA55A;
f010053c:	66 c7 05 00 80 0b f0 	movw   $0xa55a,0xf00b8000
f0100543:	5a a5 
	if (*cp != 0xA55A) {
f0100545:	0f b7 05 00 80 0b f0 	movzwl 0xf00b8000,%eax
f010054c:	66 3d 5a a5          	cmp    $0xa55a,%ax
f0100550:	74 11                	je     f0100563 <cons_init+0x37>
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
		addr_6845 = MONO_BASE;
f0100552:	c7 05 30 25 11 f0 b4 	movl   $0x3b4,0xf0112530
f0100559:	03 00 00 

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
	was = *cp;
	*cp = (uint16_t) 0xA55A;
	if (*cp != 0xA55A) {
		cp = (uint16_t*) (KERNBASE + MONO_BUF);
f010055c:	be 00 00 0b f0       	mov    $0xf00b0000,%esi
f0100561:	eb 16                	jmp    f0100579 <cons_init+0x4d>
		addr_6845 = MONO_BASE;
	} else {
		*cp = was;
f0100563:	66 89 15 00 80 0b f0 	mov    %dx,0xf00b8000
		addr_6845 = CGA_BASE;
f010056a:	c7 05 30 25 11 f0 d4 	movl   $0x3d4,0xf0112530
f0100571:	03 00 00 
{
	volatile uint16_t *cp;
	uint16_t was;
	unsigned pos;

	cp = (uint16_t*) (KERNBASE + CGA_BUF);
f0100574:	be 00 80 0b f0       	mov    $0xf00b8000,%esi
		*cp = was;
		addr_6845 = CGA_BASE;
	}

	/* Extract cursor location */
	outb(addr_6845, 14);
f0100579:	8b 3d 30 25 11 f0    	mov    0xf0112530,%edi
f010057f:	b8 0e 00 00 00       	mov    $0xe,%eax
f0100584:	89 fa                	mov    %edi,%edx
f0100586:	ee                   	out    %al,(%dx)
	pos = inb(addr_6845 + 1) << 8;
f0100587:	8d 5f 01             	lea    0x1(%edi),%ebx

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010058a:	89 da                	mov    %ebx,%edx
f010058c:	ec                   	in     (%dx),%al
f010058d:	0f b6 c8             	movzbl %al,%ecx
f0100590:	c1 e1 08             	shl    $0x8,%ecx
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f0100593:	b8 0f 00 00 00       	mov    $0xf,%eax
f0100598:	89 fa                	mov    %edi,%edx
f010059a:	ee                   	out    %al,(%dx)

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f010059b:	89 da                	mov    %ebx,%edx
f010059d:	ec                   	in     (%dx),%al
	outb(addr_6845, 15);
	pos |= inb(addr_6845 + 1);

	crt_buf = (uint16_t*) cp;
f010059e:	89 35 2c 25 11 f0    	mov    %esi,0xf011252c
	crt_pos = pos;
f01005a4:	0f b6 c0             	movzbl %al,%eax
f01005a7:	09 c8                	or     %ecx,%eax
f01005a9:	66 a3 28 25 11 f0    	mov    %ax,0xf0112528
}

static __inline void
outb(int port, uint8_t data)
{
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
f01005af:	be fa 03 00 00       	mov    $0x3fa,%esi
f01005b4:	b8 00 00 00 00       	mov    $0x0,%eax
f01005b9:	89 f2                	mov    %esi,%edx
f01005bb:	ee                   	out    %al,(%dx)
f01005bc:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005c1:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
f01005c6:	ee                   	out    %al,(%dx)
f01005c7:	bb f8 03 00 00       	mov    $0x3f8,%ebx
f01005cc:	b8 0c 00 00 00       	mov    $0xc,%eax
f01005d1:	89 da                	mov    %ebx,%edx
f01005d3:	ee                   	out    %al,(%dx)
f01005d4:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005d9:	b8 00 00 00 00       	mov    $0x0,%eax
f01005de:	ee                   	out    %al,(%dx)
f01005df:	ba fb 03 00 00       	mov    $0x3fb,%edx
f01005e4:	b8 03 00 00 00       	mov    $0x3,%eax
f01005e9:	ee                   	out    %al,(%dx)
f01005ea:	ba fc 03 00 00       	mov    $0x3fc,%edx
f01005ef:	b8 00 00 00 00       	mov    $0x0,%eax
f01005f4:	ee                   	out    %al,(%dx)
f01005f5:	ba f9 03 00 00       	mov    $0x3f9,%edx
f01005fa:	b8 01 00 00 00       	mov    $0x1,%eax
f01005ff:	ee                   	out    %al,(%dx)

static __inline uint8_t
inb(int port)
{
	uint8_t data;
	__asm __volatile("inb %w1,%0" : "=a" (data) : "d" (port));
f0100600:	ba fd 03 00 00       	mov    $0x3fd,%edx
f0100605:	ec                   	in     (%dx),%al
f0100606:	89 c1                	mov    %eax,%ecx
	// Enable rcv interrupts
	outb(COM1+COM_IER, COM_IER_RDI);

	// Clear any preexisting overrun indications and interrupts
	// Serial port doesn't exist if COM_LSR returns 0xFF
	serial_exists = (inb(COM1+COM_LSR) != 0xFF);
f0100608:	3c ff                	cmp    $0xff,%al
f010060a:	0f 95 05 34 25 11 f0 	setne  0xf0112534
f0100611:	89 f2                	mov    %esi,%edx
f0100613:	ec                   	in     (%dx),%al
f0100614:	89 da                	mov    %ebx,%edx
f0100616:	ec                   	in     (%dx),%al
{
	cga_init();
	kbd_init();
	serial_init();

	if (!serial_exists)
f0100617:	80 f9 ff             	cmp    $0xff,%cl
f010061a:	75 10                	jne    f010062c <cons_init+0x100>
		cprintf("Serial port does not exist!\n");
f010061c:	83 ec 0c             	sub    $0xc,%esp
f010061f:	68 70 19 10 f0       	push   $0xf0101970
f0100624:	e8 59 03 00 00       	call   f0100982 <cprintf>
f0100629:	83 c4 10             	add    $0x10,%esp
}
f010062c:	8d 65 f4             	lea    -0xc(%ebp),%esp
f010062f:	5b                   	pop    %ebx
f0100630:	5e                   	pop    %esi
f0100631:	5f                   	pop    %edi
f0100632:	5d                   	pop    %ebp
f0100633:	c3                   	ret    

f0100634 <cputchar>:

// `High'-level console I/O.  Used by readline and cprintf.

void
cputchar(int c)
{
f0100634:	55                   	push   %ebp
f0100635:	89 e5                	mov    %esp,%ebp
f0100637:	83 ec 08             	sub    $0x8,%esp
	cons_putc(c);
f010063a:	8b 45 08             	mov    0x8(%ebp),%eax
f010063d:	e8 9d fc ff ff       	call   f01002df <cons_putc>
}
f0100642:	c9                   	leave  
f0100643:	c3                   	ret    

f0100644 <getchar>:

int
getchar(void)
{
f0100644:	55                   	push   %ebp
f0100645:	89 e5                	mov    %esp,%ebp
f0100647:	83 ec 08             	sub    $0x8,%esp
	int c;

	while ((c = cons_getc()) == 0)
f010064a:	e8 93 fe ff ff       	call   f01004e2 <cons_getc>
f010064f:	85 c0                	test   %eax,%eax
f0100651:	74 f7                	je     f010064a <getchar+0x6>
		/* do nothing */;
	return c;
}
f0100653:	c9                   	leave  
f0100654:	c3                   	ret    

f0100655 <iscons>:

int
iscons(int fdnum)
{
f0100655:	55                   	push   %ebp
f0100656:	89 e5                	mov    %esp,%ebp
	// used by readline
	return 1;
}
f0100658:	b8 01 00 00 00       	mov    $0x1,%eax
f010065d:	5d                   	pop    %ebp
f010065e:	c3                   	ret    

f010065f <mon_help>:

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
f010065f:	55                   	push   %ebp
f0100660:	89 e5                	mov    %esp,%ebp
f0100662:	83 ec 0c             	sub    $0xc,%esp
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
f0100665:	68 c0 1b 10 f0       	push   $0xf0101bc0
f010066a:	68 de 1b 10 f0       	push   $0xf0101bde
f010066f:	68 e3 1b 10 f0       	push   $0xf0101be3
f0100674:	e8 09 03 00 00       	call   f0100982 <cprintf>
f0100679:	83 c4 0c             	add    $0xc,%esp
f010067c:	68 b4 1c 10 f0       	push   $0xf0101cb4
f0100681:	68 ec 1b 10 f0       	push   $0xf0101bec
f0100686:	68 e3 1b 10 f0       	push   $0xf0101be3
f010068b:	e8 f2 02 00 00       	call   f0100982 <cprintf>
f0100690:	83 c4 0c             	add    $0xc,%esp
f0100693:	68 dc 1c 10 f0       	push   $0xf0101cdc
f0100698:	68 f5 1b 10 f0       	push   $0xf0101bf5
f010069d:	68 e3 1b 10 f0       	push   $0xf0101be3
f01006a2:	e8 db 02 00 00       	call   f0100982 <cprintf>
	return 0;
}
f01006a7:	b8 00 00 00 00       	mov    $0x0,%eax
f01006ac:	c9                   	leave  
f01006ad:	c3                   	ret    

f01006ae <mon_kerninfo>:

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
f01006ae:	55                   	push   %ebp
f01006af:	89 e5                	mov    %esp,%ebp
f01006b1:	83 ec 14             	sub    $0x14,%esp
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
f01006b4:	68 ff 1b 10 f0       	push   $0xf0101bff
f01006b9:	e8 c4 02 00 00       	call   f0100982 <cprintf>
	cprintf("  _start                  %08x (phys)\n", _start);
f01006be:	83 c4 08             	add    $0x8,%esp
f01006c1:	68 0c 00 10 00       	push   $0x10000c
f01006c6:	68 00 1d 10 f0       	push   $0xf0101d00
f01006cb:	e8 b2 02 00 00       	call   f0100982 <cprintf>
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
f01006d0:	83 c4 0c             	add    $0xc,%esp
f01006d3:	68 0c 00 10 00       	push   $0x10000c
f01006d8:	68 0c 00 10 f0       	push   $0xf010000c
f01006dd:	68 28 1d 10 f0       	push   $0xf0101d28
f01006e2:	e8 9b 02 00 00       	call   f0100982 <cprintf>
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
f01006e7:	83 c4 0c             	add    $0xc,%esp
f01006ea:	68 c1 18 10 00       	push   $0x1018c1
f01006ef:	68 c1 18 10 f0       	push   $0xf01018c1
f01006f4:	68 4c 1d 10 f0       	push   $0xf0101d4c
f01006f9:	e8 84 02 00 00       	call   f0100982 <cprintf>
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
f01006fe:	83 c4 0c             	add    $0xc,%esp
f0100701:	68 00 23 11 00       	push   $0x112300
f0100706:	68 00 23 11 f0       	push   $0xf0112300
f010070b:	68 70 1d 10 f0       	push   $0xf0101d70
f0100710:	e8 6d 02 00 00       	call   f0100982 <cprintf>
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
f0100715:	83 c4 0c             	add    $0xc,%esp
f0100718:	68 44 29 11 00       	push   $0x112944
f010071d:	68 44 29 11 f0       	push   $0xf0112944
f0100722:	68 94 1d 10 f0       	push   $0xf0101d94
f0100727:	e8 56 02 00 00       	call   f0100982 <cprintf>
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
f010072c:	b8 43 2d 11 f0       	mov    $0xf0112d43,%eax
f0100731:	2d 0c 00 10 f0       	sub    $0xf010000c,%eax
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
f0100736:	83 c4 08             	add    $0x8,%esp
f0100739:	c1 f8 0a             	sar    $0xa,%eax
f010073c:	50                   	push   %eax
f010073d:	68 b8 1d 10 f0       	push   $0xf0101db8
f0100742:	e8 3b 02 00 00       	call   f0100982 <cprintf>
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}
f0100747:	b8 00 00 00 00       	mov    $0x0,%eax
f010074c:	c9                   	leave  
f010074d:	c3                   	ret    

f010074e <mon_backtrace>:

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
f010074e:	55                   	push   %ebp
f010074f:	89 e5                	mov    %esp,%ebp
f0100751:	57                   	push   %edi
f0100752:	56                   	push   %esi
f0100753:	53                   	push   %ebx
f0100754:	83 ec 38             	sub    $0x38,%esp
	// Your code here.
    cprintf("Stack backtrace:\n");
f0100757:	68 18 1c 10 f0       	push   $0xf0101c18
f010075c:	e8 21 02 00 00       	call   f0100982 <cprintf>

static __inline uint32_t
read_ebp(void)
{
	uint32_t ebp;
	__asm __volatile("movl %%ebp,%0" : "=r" (ebp));
f0100761:	89 ee                	mov    %ebp,%esi
    struct Eipdebuginfo info;
    uint32_t* eip;
    uint32_t* ebp = (uint32_t*)read_ebp(); // Get current ebp
    // if ebp is 0, stack backtraces will be terminated properly.
    while(ebp){
f0100763:	83 c4 10             	add    $0x10,%esp
            cprintf(" %08x", *(ebp + i));
        }
        cprintf("\n");
        ebp = (uint32_t*)*ebp;
        // Get function information by eip.
        if (debuginfo_eip(*eip, &info) != 0) {
f0100766:	8d 7d d0             	lea    -0x30(%ebp),%edi
    cprintf("Stack backtrace:\n");
    struct Eipdebuginfo info;
    uint32_t* eip;
    uint32_t* ebp = (uint32_t*)read_ebp(); // Get current ebp
    // if ebp is 0, stack backtraces will be terminated properly.
    while(ebp){
f0100769:	e9 92 00 00 00       	jmp    f0100800 <mon_backtrace+0xb2>
        eip = ebp + 1;
        cprintf("  ebp %08x  eip %08x  args", ebp, *eip);
f010076e:	83 ec 04             	sub    $0x4,%esp
f0100771:	ff 76 04             	pushl  0x4(%esi)
f0100774:	56                   	push   %esi
f0100775:	68 2a 1c 10 f0       	push   $0xf0101c2a
f010077a:	e8 03 02 00 00       	call   f0100982 <cprintf>
f010077f:	83 c4 10             	add    $0x10,%esp
        int i;
        for (i = 2; i <= 5; i++) {
f0100782:	bb 02 00 00 00       	mov    $0x2,%ebx
            cprintf(" %08x", *(ebp + i));
f0100787:	83 ec 08             	sub    $0x8,%esp
f010078a:	ff 34 9e             	pushl  (%esi,%ebx,4)
f010078d:	68 45 1c 10 f0       	push   $0xf0101c45
f0100792:	e8 eb 01 00 00       	call   f0100982 <cprintf>
    // if ebp is 0, stack backtraces will be terminated properly.
    while(ebp){
        eip = ebp + 1;
        cprintf("  ebp %08x  eip %08x  args", ebp, *eip);
        int i;
        for (i = 2; i <= 5; i++) {
f0100797:	83 c3 01             	add    $0x1,%ebx
f010079a:	83 c4 10             	add    $0x10,%esp
f010079d:	83 fb 06             	cmp    $0x6,%ebx
f01007a0:	75 e5                	jne    f0100787 <mon_backtrace+0x39>
            cprintf(" %08x", *(ebp + i));
        }
        cprintf("\n");
f01007a2:	83 ec 0c             	sub    $0xc,%esp
f01007a5:	68 6e 19 10 f0       	push   $0xf010196e
f01007aa:	e8 d3 01 00 00       	call   f0100982 <cprintf>
        ebp = (uint32_t*)*ebp;
f01007af:	8b 1e                	mov    (%esi),%ebx
        // Get function information by eip.
        if (debuginfo_eip(*eip, &info) != 0) {
f01007b1:	83 c4 08             	add    $0x8,%esp
f01007b4:	57                   	push   %edi
f01007b5:	ff 76 04             	pushl  0x4(%esi)
f01007b8:	e8 cf 02 00 00       	call   f0100a8c <debuginfo_eip>
f01007bd:	83 c4 10             	add    $0x10,%esp
f01007c0:	85 c0                	test   %eax,%eax
f01007c2:	74 17                	je     f01007db <mon_backtrace+0x8d>
            cprintf("debug info error!\n");
f01007c4:	83 ec 0c             	sub    $0xc,%esp
f01007c7:	68 4b 1c 10 f0       	push   $0xf0101c4b
f01007cc:	e8 b1 01 00 00       	call   f0100982 <cprintf>
            return -1;
f01007d1:	83 c4 10             	add    $0x10,%esp
f01007d4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f01007d9:	eb 32                	jmp    f010080d <mon_backtrace+0xbf>
        }
        cprintf("       %s:%d: %.*s+%d\n", info.eip_file, info.eip_line, info.eip_fn_namelen, info.eip_fn_name, *eip - info.eip_fn_addr);
f01007db:	83 ec 08             	sub    $0x8,%esp
f01007de:	8b 46 04             	mov    0x4(%esi),%eax
f01007e1:	2b 45 e0             	sub    -0x20(%ebp),%eax
f01007e4:	50                   	push   %eax
f01007e5:	ff 75 d8             	pushl  -0x28(%ebp)
f01007e8:	ff 75 dc             	pushl  -0x24(%ebp)
f01007eb:	ff 75 d4             	pushl  -0x2c(%ebp)
f01007ee:	ff 75 d0             	pushl  -0x30(%ebp)
f01007f1:	68 5e 1c 10 f0       	push   $0xf0101c5e
f01007f6:	e8 87 01 00 00       	call   f0100982 <cprintf>
f01007fb:	83 c4 20             	add    $0x20,%esp
        int i;
        for (i = 2; i <= 5; i++) {
            cprintf(" %08x", *(ebp + i));
        }
        cprintf("\n");
        ebp = (uint32_t*)*ebp;
f01007fe:	89 de                	mov    %ebx,%esi
    cprintf("Stack backtrace:\n");
    struct Eipdebuginfo info;
    uint32_t* eip;
    uint32_t* ebp = (uint32_t*)read_ebp(); // Get current ebp
    // if ebp is 0, stack backtraces will be terminated properly.
    while(ebp){
f0100800:	85 f6                	test   %esi,%esi
f0100802:	0f 85 66 ff ff ff    	jne    f010076e <mon_backtrace+0x20>
            cprintf("debug info error!\n");
            return -1;
        }
        cprintf("       %s:%d: %.*s+%d\n", info.eip_file, info.eip_line, info.eip_fn_namelen, info.eip_fn_name, *eip - info.eip_fn_addr);
    }
	return 0;
f0100808:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010080d:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100810:	5b                   	pop    %ebx
f0100811:	5e                   	pop    %esi
f0100812:	5f                   	pop    %edi
f0100813:	5d                   	pop    %ebp
f0100814:	c3                   	ret    

f0100815 <monitor>:
	return 0;
}

void
monitor(struct Trapframe *tf)
{
f0100815:	55                   	push   %ebp
f0100816:	89 e5                	mov    %esp,%ebp
f0100818:	57                   	push   %edi
f0100819:	56                   	push   %esi
f010081a:	53                   	push   %ebx
f010081b:	83 ec 58             	sub    $0x58,%esp
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
f010081e:	68 e4 1d 10 f0       	push   $0xf0101de4
f0100823:	e8 5a 01 00 00       	call   f0100982 <cprintf>
	cprintf("Type 'help' for a list of commands.\n");
f0100828:	c7 04 24 08 1e 10 f0 	movl   $0xf0101e08,(%esp)
f010082f:	e8 4e 01 00 00       	call   f0100982 <cprintf>
f0100834:	83 c4 10             	add    $0x10,%esp
	// cprintf("H%x Wo%s", 57616, &i);

	// cprintf("x=%d y=%d", 3);

	while (1) {
		buf = readline("K> ");
f0100837:	83 ec 0c             	sub    $0xc,%esp
f010083a:	68 75 1c 10 f0       	push   $0xf0101c75
f010083f:	e8 9d 09 00 00       	call   f01011e1 <readline>
f0100844:	89 c3                	mov    %eax,%ebx
		if (buf != NULL)
f0100846:	83 c4 10             	add    $0x10,%esp
f0100849:	85 c0                	test   %eax,%eax
f010084b:	74 ea                	je     f0100837 <monitor+0x22>
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
f010084d:	c7 45 a8 00 00 00 00 	movl   $0x0,-0x58(%ebp)
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
f0100854:	be 00 00 00 00       	mov    $0x0,%esi
f0100859:	eb 0a                	jmp    f0100865 <monitor+0x50>
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
f010085b:	c6 03 00             	movb   $0x0,(%ebx)
f010085e:	89 f7                	mov    %esi,%edi
f0100860:	8d 5b 01             	lea    0x1(%ebx),%ebx
f0100863:	89 fe                	mov    %edi,%esi
	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
f0100865:	0f b6 03             	movzbl (%ebx),%eax
f0100868:	84 c0                	test   %al,%al
f010086a:	74 63                	je     f01008cf <monitor+0xba>
f010086c:	83 ec 08             	sub    $0x8,%esp
f010086f:	0f be c0             	movsbl %al,%eax
f0100872:	50                   	push   %eax
f0100873:	68 79 1c 10 f0       	push   $0xf0101c79
f0100878:	e8 7e 0b 00 00       	call   f01013fb <strchr>
f010087d:	83 c4 10             	add    $0x10,%esp
f0100880:	85 c0                	test   %eax,%eax
f0100882:	75 d7                	jne    f010085b <monitor+0x46>
			*buf++ = 0;
		if (*buf == 0)
f0100884:	80 3b 00             	cmpb   $0x0,(%ebx)
f0100887:	74 46                	je     f01008cf <monitor+0xba>
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
f0100889:	83 fe 0f             	cmp    $0xf,%esi
f010088c:	75 14                	jne    f01008a2 <monitor+0x8d>
			cprintf("Too many arguments (max %d)\n", MAXARGS);
f010088e:	83 ec 08             	sub    $0x8,%esp
f0100891:	6a 10                	push   $0x10
f0100893:	68 7e 1c 10 f0       	push   $0xf0101c7e
f0100898:	e8 e5 00 00 00       	call   f0100982 <cprintf>
f010089d:	83 c4 10             	add    $0x10,%esp
f01008a0:	eb 95                	jmp    f0100837 <monitor+0x22>
			return 0;
		}
		argv[argc++] = buf;
f01008a2:	8d 7e 01             	lea    0x1(%esi),%edi
f01008a5:	89 5c b5 a8          	mov    %ebx,-0x58(%ebp,%esi,4)
f01008a9:	eb 03                	jmp    f01008ae <monitor+0x99>
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
f01008ab:	83 c3 01             	add    $0x1,%ebx
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
f01008ae:	0f b6 03             	movzbl (%ebx),%eax
f01008b1:	84 c0                	test   %al,%al
f01008b3:	74 ae                	je     f0100863 <monitor+0x4e>
f01008b5:	83 ec 08             	sub    $0x8,%esp
f01008b8:	0f be c0             	movsbl %al,%eax
f01008bb:	50                   	push   %eax
f01008bc:	68 79 1c 10 f0       	push   $0xf0101c79
f01008c1:	e8 35 0b 00 00       	call   f01013fb <strchr>
f01008c6:	83 c4 10             	add    $0x10,%esp
f01008c9:	85 c0                	test   %eax,%eax
f01008cb:	74 de                	je     f01008ab <monitor+0x96>
f01008cd:	eb 94                	jmp    f0100863 <monitor+0x4e>
			buf++;
	}
	argv[argc] = 0;
f01008cf:	c7 44 b5 a8 00 00 00 	movl   $0x0,-0x58(%ebp,%esi,4)
f01008d6:	00 

	// Lookup and invoke the command
	if (argc == 0)
f01008d7:	85 f6                	test   %esi,%esi
f01008d9:	0f 84 58 ff ff ff    	je     f0100837 <monitor+0x22>
f01008df:	bb 00 00 00 00       	mov    $0x0,%ebx
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
f01008e4:	83 ec 08             	sub    $0x8,%esp
f01008e7:	8d 04 5b             	lea    (%ebx,%ebx,2),%eax
f01008ea:	ff 34 85 40 1e 10 f0 	pushl  -0xfefe1c0(,%eax,4)
f01008f1:	ff 75 a8             	pushl  -0x58(%ebp)
f01008f4:	e8 a4 0a 00 00       	call   f010139d <strcmp>
f01008f9:	83 c4 10             	add    $0x10,%esp
f01008fc:	85 c0                	test   %eax,%eax
f01008fe:	75 21                	jne    f0100921 <monitor+0x10c>
			return commands[i].func(argc, argv, tf);
f0100900:	83 ec 04             	sub    $0x4,%esp
f0100903:	8d 04 5b             	lea    (%ebx,%ebx,2),%eax
f0100906:	ff 75 08             	pushl  0x8(%ebp)
f0100909:	8d 55 a8             	lea    -0x58(%ebp),%edx
f010090c:	52                   	push   %edx
f010090d:	56                   	push   %esi
f010090e:	ff 14 85 48 1e 10 f0 	call   *-0xfefe1b8(,%eax,4)
	// cprintf("x=%d y=%d", 3);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
f0100915:	83 c4 10             	add    $0x10,%esp
f0100918:	85 c0                	test   %eax,%eax
f010091a:	78 25                	js     f0100941 <monitor+0x12c>
f010091c:	e9 16 ff ff ff       	jmp    f0100837 <monitor+0x22>
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
f0100921:	83 c3 01             	add    $0x1,%ebx
f0100924:	83 fb 03             	cmp    $0x3,%ebx
f0100927:	75 bb                	jne    f01008e4 <monitor+0xcf>
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
f0100929:	83 ec 08             	sub    $0x8,%esp
f010092c:	ff 75 a8             	pushl  -0x58(%ebp)
f010092f:	68 9b 1c 10 f0       	push   $0xf0101c9b
f0100934:	e8 49 00 00 00       	call   f0100982 <cprintf>
f0100939:	83 c4 10             	add    $0x10,%esp
f010093c:	e9 f6 fe ff ff       	jmp    f0100837 <monitor+0x22>
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
f0100941:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100944:	5b                   	pop    %ebx
f0100945:	5e                   	pop    %esi
f0100946:	5f                   	pop    %edi
f0100947:	5d                   	pop    %ebp
f0100948:	c3                   	ret    

f0100949 <putch>:
#include <inc/stdarg.h>


static void
putch(int ch, int *cnt)
{
f0100949:	55                   	push   %ebp
f010094a:	89 e5                	mov    %esp,%ebp
f010094c:	83 ec 14             	sub    $0x14,%esp
	cputchar(ch);
f010094f:	ff 75 08             	pushl  0x8(%ebp)
f0100952:	e8 dd fc ff ff       	call   f0100634 <cputchar>
	*cnt++;
}
f0100957:	83 c4 10             	add    $0x10,%esp
f010095a:	c9                   	leave  
f010095b:	c3                   	ret    

f010095c <vcprintf>:

int
vcprintf(const char *fmt, va_list ap)
{
f010095c:	55                   	push   %ebp
f010095d:	89 e5                	mov    %esp,%ebp
f010095f:	83 ec 18             	sub    $0x18,%esp
	int cnt = 0;
f0100962:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	vprintfmt((void*)putch, &cnt, fmt, ap);
f0100969:	ff 75 0c             	pushl  0xc(%ebp)
f010096c:	ff 75 08             	pushl  0x8(%ebp)
f010096f:	8d 45 f4             	lea    -0xc(%ebp),%eax
f0100972:	50                   	push   %eax
f0100973:	68 49 09 10 f0       	push   $0xf0100949
f0100978:	e8 52 04 00 00       	call   f0100dcf <vprintfmt>
	return cnt;
}
f010097d:	8b 45 f4             	mov    -0xc(%ebp),%eax
f0100980:	c9                   	leave  
f0100981:	c3                   	ret    

f0100982 <cprintf>:

int
cprintf(const char *fmt, ...)
{
f0100982:	55                   	push   %ebp
f0100983:	89 e5                	mov    %esp,%ebp
f0100985:	83 ec 10             	sub    $0x10,%esp
	va_list ap;
	int cnt;

	va_start(ap, fmt);
f0100988:	8d 45 0c             	lea    0xc(%ebp),%eax
	cnt = vcprintf(fmt, ap);
f010098b:	50                   	push   %eax
f010098c:	ff 75 08             	pushl  0x8(%ebp)
f010098f:	e8 c8 ff ff ff       	call   f010095c <vcprintf>
	va_end(ap);

	return cnt;
}
f0100994:	c9                   	leave  
f0100995:	c3                   	ret    

f0100996 <stab_binsearch>:
//	will exit setting left = 118, right = 554.
//
static void
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
f0100996:	55                   	push   %ebp
f0100997:	89 e5                	mov    %esp,%ebp
f0100999:	57                   	push   %edi
f010099a:	56                   	push   %esi
f010099b:	53                   	push   %ebx
f010099c:	83 ec 14             	sub    $0x14,%esp
f010099f:	89 45 ec             	mov    %eax,-0x14(%ebp)
f01009a2:	89 55 e4             	mov    %edx,-0x1c(%ebp)
f01009a5:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f01009a8:	8b 7d 08             	mov    0x8(%ebp),%edi
	int l = *region_left, r = *region_right, any_matches = 0;
f01009ab:	8b 1a                	mov    (%edx),%ebx
f01009ad:	8b 01                	mov    (%ecx),%eax
f01009af:	89 45 f0             	mov    %eax,-0x10(%ebp)
f01009b2:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)

	while (l <= r) {
f01009b9:	eb 7f                	jmp    f0100a3a <stab_binsearch+0xa4>
		int true_m = (l + r) / 2, m = true_m;
f01009bb:	8b 45 f0             	mov    -0x10(%ebp),%eax
f01009be:	01 d8                	add    %ebx,%eax
f01009c0:	89 c6                	mov    %eax,%esi
f01009c2:	c1 ee 1f             	shr    $0x1f,%esi
f01009c5:	01 c6                	add    %eax,%esi
f01009c7:	d1 fe                	sar    %esi
f01009c9:	8d 04 76             	lea    (%esi,%esi,2),%eax
f01009cc:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01009cf:	8d 14 81             	lea    (%ecx,%eax,4),%edx
f01009d2:	89 f0                	mov    %esi,%eax

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01009d4:	eb 03                	jmp    f01009d9 <stab_binsearch+0x43>
			m--;
f01009d6:	83 e8 01             	sub    $0x1,%eax

	while (l <= r) {
		int true_m = (l + r) / 2, m = true_m;

		// search for earliest stab with right type
		while (m >= l && stabs[m].n_type != type)
f01009d9:	39 c3                	cmp    %eax,%ebx
f01009db:	7f 0d                	jg     f01009ea <stab_binsearch+0x54>
f01009dd:	0f b6 4a 04          	movzbl 0x4(%edx),%ecx
f01009e1:	83 ea 0c             	sub    $0xc,%edx
f01009e4:	39 f9                	cmp    %edi,%ecx
f01009e6:	75 ee                	jne    f01009d6 <stab_binsearch+0x40>
f01009e8:	eb 05                	jmp    f01009ef <stab_binsearch+0x59>
			m--;
		if (m < l) {	// no match in [l, m]
			l = true_m + 1;
f01009ea:	8d 5e 01             	lea    0x1(%esi),%ebx
			continue;
f01009ed:	eb 4b                	jmp    f0100a3a <stab_binsearch+0xa4>
		}

		// actual binary search
		any_matches = 1;
		if (stabs[m].n_value < addr) {
f01009ef:	8d 14 40             	lea    (%eax,%eax,2),%edx
f01009f2:	8b 4d ec             	mov    -0x14(%ebp),%ecx
f01009f5:	8b 54 91 08          	mov    0x8(%ecx,%edx,4),%edx
f01009f9:	39 55 0c             	cmp    %edx,0xc(%ebp)
f01009fc:	76 11                	jbe    f0100a0f <stab_binsearch+0x79>
			*region_left = m;
f01009fe:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
f0100a01:	89 03                	mov    %eax,(%ebx)
			l = true_m + 1;
f0100a03:	8d 5e 01             	lea    0x1(%esi),%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100a06:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0100a0d:	eb 2b                	jmp    f0100a3a <stab_binsearch+0xa4>
		if (stabs[m].n_value < addr) {
			*region_left = m;
			l = true_m + 1;
		} else if (stabs[m].n_value > addr) {
f0100a0f:	39 55 0c             	cmp    %edx,0xc(%ebp)
f0100a12:	73 14                	jae    f0100a28 <stab_binsearch+0x92>
			*region_right = m - 1;
f0100a14:	83 e8 01             	sub    $0x1,%eax
f0100a17:	89 45 f0             	mov    %eax,-0x10(%ebp)
f0100a1a:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0100a1d:	89 06                	mov    %eax,(%esi)
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100a1f:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
f0100a26:	eb 12                	jmp    f0100a3a <stab_binsearch+0xa4>
			*region_right = m - 1;
			r = m - 1;
		} else {
			// exact match for 'addr', but continue loop to find
			// *region_right
			*region_left = m;
f0100a28:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100a2b:	89 06                	mov    %eax,(%esi)
			l = m;
			addr++;
f0100a2d:	83 45 0c 01          	addl   $0x1,0xc(%ebp)
f0100a31:	89 c3                	mov    %eax,%ebx
			l = true_m + 1;
			continue;
		}

		// actual binary search
		any_matches = 1;
f0100a33:	c7 45 e8 01 00 00 00 	movl   $0x1,-0x18(%ebp)
stab_binsearch(const struct Stab *stabs, int *region_left, int *region_right,
	       int type, uintptr_t addr)
{
	int l = *region_left, r = *region_right, any_matches = 0;

	while (l <= r) {
f0100a3a:	3b 5d f0             	cmp    -0x10(%ebp),%ebx
f0100a3d:	0f 8e 78 ff ff ff    	jle    f01009bb <stab_binsearch+0x25>
			l = m;
			addr++;
		}
	}

	if (!any_matches)
f0100a43:	83 7d e8 00          	cmpl   $0x0,-0x18(%ebp)
f0100a47:	75 0f                	jne    f0100a58 <stab_binsearch+0xc2>
		*region_right = *region_left - 1;
f0100a49:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100a4c:	8b 00                	mov    (%eax),%eax
f0100a4e:	83 e8 01             	sub    $0x1,%eax
f0100a51:	8b 75 e0             	mov    -0x20(%ebp),%esi
f0100a54:	89 06                	mov    %eax,(%esi)
f0100a56:	eb 2c                	jmp    f0100a84 <stab_binsearch+0xee>
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100a58:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100a5b:	8b 00                	mov    (%eax),%eax
		     l > *region_left && stabs[l].n_type != type;
f0100a5d:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100a60:	8b 0e                	mov    (%esi),%ecx
f0100a62:	8d 14 40             	lea    (%eax,%eax,2),%edx
f0100a65:	8b 75 ec             	mov    -0x14(%ebp),%esi
f0100a68:	8d 14 96             	lea    (%esi,%edx,4),%edx

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100a6b:	eb 03                	jmp    f0100a70 <stab_binsearch+0xda>
		     l > *region_left && stabs[l].n_type != type;
		     l--)
f0100a6d:	83 e8 01             	sub    $0x1,%eax

	if (!any_matches)
		*region_right = *region_left - 1;
	else {
		// find rightmost region containing 'addr'
		for (l = *region_right;
f0100a70:	39 c8                	cmp    %ecx,%eax
f0100a72:	7e 0b                	jle    f0100a7f <stab_binsearch+0xe9>
		     l > *region_left && stabs[l].n_type != type;
f0100a74:	0f b6 5a 04          	movzbl 0x4(%edx),%ebx
f0100a78:	83 ea 0c             	sub    $0xc,%edx
f0100a7b:	39 df                	cmp    %ebx,%edi
f0100a7d:	75 ee                	jne    f0100a6d <stab_binsearch+0xd7>
		     l--)
			/* do nothing */;
		*region_left = l;
f0100a7f:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100a82:	89 06                	mov    %eax,(%esi)
	}
}
f0100a84:	83 c4 14             	add    $0x14,%esp
f0100a87:	5b                   	pop    %ebx
f0100a88:	5e                   	pop    %esi
f0100a89:	5f                   	pop    %edi
f0100a8a:	5d                   	pop    %ebp
f0100a8b:	c3                   	ret    

f0100a8c <debuginfo_eip>:
//	negative if not.  But even if it returns negative it has stored some
//	information into '*info'.
//
int
debuginfo_eip(uintptr_t addr, struct Eipdebuginfo *info)
{
f0100a8c:	55                   	push   %ebp
f0100a8d:	89 e5                	mov    %esp,%ebp
f0100a8f:	57                   	push   %edi
f0100a90:	56                   	push   %esi
f0100a91:	53                   	push   %ebx
f0100a92:	83 ec 3c             	sub    $0x3c,%esp
f0100a95:	8b 75 08             	mov    0x8(%ebp),%esi
f0100a98:	8b 5d 0c             	mov    0xc(%ebp),%ebx
	const struct Stab *stabs, *stab_end;
	const char *stabstr, *stabstr_end;
	int lfile, rfile, lfun, rfun, lline, rline;

	// Initialize *info
	info->eip_file = "<unknown>";
f0100a9b:	c7 03 64 1e 10 f0    	movl   $0xf0101e64,(%ebx)
	info->eip_line = 0;
f0100aa1:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
	info->eip_fn_name = "<unknown>";
f0100aa8:	c7 43 08 64 1e 10 f0 	movl   $0xf0101e64,0x8(%ebx)
	info->eip_fn_namelen = 9;
f0100aaf:	c7 43 0c 09 00 00 00 	movl   $0x9,0xc(%ebx)
	info->eip_fn_addr = addr;
f0100ab6:	89 73 10             	mov    %esi,0x10(%ebx)
	info->eip_fn_narg = 0;
f0100ab9:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)

	// Find the relevant set of stabs
	if (addr >= ULIM) {
f0100ac0:	81 fe ff ff 7f ef    	cmp    $0xef7fffff,%esi
f0100ac6:	76 11                	jbe    f0100ad9 <debuginfo_eip+0x4d>
		// Can't search for user-level addresses yet!
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100ac8:	b8 f3 73 10 f0       	mov    $0xf01073f3,%eax
f0100acd:	3d dd 5a 10 f0       	cmp    $0xf0105add,%eax
f0100ad2:	77 19                	ja     f0100aed <debuginfo_eip+0x61>
f0100ad4:	e9 aa 01 00 00       	jmp    f0100c83 <debuginfo_eip+0x1f7>
		stab_end = __STAB_END__;
		stabstr = __STABSTR_BEGIN__;
		stabstr_end = __STABSTR_END__;
	} else {
		// Can't search for user-level addresses yet!
  	        panic("User address");
f0100ad9:	83 ec 04             	sub    $0x4,%esp
f0100adc:	68 6e 1e 10 f0       	push   $0xf0101e6e
f0100ae1:	6a 7f                	push   $0x7f
f0100ae3:	68 7b 1e 10 f0       	push   $0xf0101e7b
f0100ae8:	e8 f9 f5 ff ff       	call   f01000e6 <_panic>
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
f0100aed:	80 3d f2 73 10 f0 00 	cmpb   $0x0,0xf01073f2
f0100af4:	0f 85 90 01 00 00    	jne    f0100c8a <debuginfo_eip+0x1fe>
	// 'eip'.  First, we find the basic source file containing 'eip'.
	// Then, we look in that source file for the function.  Then we look
	// for the line number.

	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
f0100afa:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
	rfile = (stab_end - stabs) - 1;
f0100b01:	b8 dc 5a 10 f0       	mov    $0xf0105adc,%eax
f0100b06:	2d b0 20 10 f0       	sub    $0xf01020b0,%eax
f0100b0b:	c1 f8 02             	sar    $0x2,%eax
f0100b0e:	69 c0 ab aa aa aa    	imul   $0xaaaaaaab,%eax,%eax
f0100b14:	83 e8 01             	sub    $0x1,%eax
f0100b17:	89 45 e0             	mov    %eax,-0x20(%ebp)
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
f0100b1a:	83 ec 08             	sub    $0x8,%esp
f0100b1d:	56                   	push   %esi
f0100b1e:	6a 64                	push   $0x64
f0100b20:	8d 4d e0             	lea    -0x20(%ebp),%ecx
f0100b23:	8d 55 e4             	lea    -0x1c(%ebp),%edx
f0100b26:	b8 b0 20 10 f0       	mov    $0xf01020b0,%eax
f0100b2b:	e8 66 fe ff ff       	call   f0100996 <stab_binsearch>
	if (lfile == 0)
f0100b30:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100b33:	83 c4 10             	add    $0x10,%esp
f0100b36:	85 c0                	test   %eax,%eax
f0100b38:	0f 84 53 01 00 00    	je     f0100c91 <debuginfo_eip+0x205>
		return -1;

	// Search within that file's stabs for the function definition
	// (N_FUN).
	lfun = lfile;
f0100b3e:	89 45 dc             	mov    %eax,-0x24(%ebp)
	rfun = rfile;
f0100b41:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100b44:	89 45 d8             	mov    %eax,-0x28(%ebp)
	stab_binsearch(stabs, &lfun, &rfun, N_FUN, addr);
f0100b47:	83 ec 08             	sub    $0x8,%esp
f0100b4a:	56                   	push   %esi
f0100b4b:	6a 24                	push   $0x24
f0100b4d:	8d 4d d8             	lea    -0x28(%ebp),%ecx
f0100b50:	8d 55 dc             	lea    -0x24(%ebp),%edx
f0100b53:	b8 b0 20 10 f0       	mov    $0xf01020b0,%eax
f0100b58:	e8 39 fe ff ff       	call   f0100996 <stab_binsearch>

	if (lfun <= rfun) {
f0100b5d:	8b 45 dc             	mov    -0x24(%ebp),%eax
f0100b60:	8b 55 d8             	mov    -0x28(%ebp),%edx
f0100b63:	83 c4 10             	add    $0x10,%esp
f0100b66:	39 d0                	cmp    %edx,%eax
f0100b68:	7f 40                	jg     f0100baa <debuginfo_eip+0x11e>
		// stabs[lfun] points to the function name
		// in the string table, but check bounds just in case.
		if (stabs[lfun].n_strx < stabstr_end - stabstr)
f0100b6a:	8d 0c 40             	lea    (%eax,%eax,2),%ecx
f0100b6d:	c1 e1 02             	shl    $0x2,%ecx
f0100b70:	8d b9 b0 20 10 f0    	lea    -0xfefdf50(%ecx),%edi
f0100b76:	89 7d c4             	mov    %edi,-0x3c(%ebp)
f0100b79:	8b b9 b0 20 10 f0    	mov    -0xfefdf50(%ecx),%edi
f0100b7f:	b9 f3 73 10 f0       	mov    $0xf01073f3,%ecx
f0100b84:	81 e9 dd 5a 10 f0    	sub    $0xf0105add,%ecx
f0100b8a:	39 cf                	cmp    %ecx,%edi
f0100b8c:	73 09                	jae    f0100b97 <debuginfo_eip+0x10b>
			info->eip_fn_name = stabstr + stabs[lfun].n_strx;
f0100b8e:	81 c7 dd 5a 10 f0    	add    $0xf0105add,%edi
f0100b94:	89 7b 08             	mov    %edi,0x8(%ebx)
		info->eip_fn_addr = stabs[lfun].n_value;
f0100b97:	8b 7d c4             	mov    -0x3c(%ebp),%edi
f0100b9a:	8b 4f 08             	mov    0x8(%edi),%ecx
f0100b9d:	89 4b 10             	mov    %ecx,0x10(%ebx)
		addr -= info->eip_fn_addr;
f0100ba0:	29 ce                	sub    %ecx,%esi
		// Search within the function definition for the line number.
		lline = lfun;
f0100ba2:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfun;
f0100ba5:	89 55 d0             	mov    %edx,-0x30(%ebp)
f0100ba8:	eb 0f                	jmp    f0100bb9 <debuginfo_eip+0x12d>
	} else {
		// Couldn't find function stab!  Maybe we're in an assembly
		// file.  Search the whole file for the line number.
		info->eip_fn_addr = addr;
f0100baa:	89 73 10             	mov    %esi,0x10(%ebx)
		lline = lfile;
f0100bad:	8b 45 e4             	mov    -0x1c(%ebp),%eax
f0100bb0:	89 45 d4             	mov    %eax,-0x2c(%ebp)
		rline = rfile;
f0100bb3:	8b 45 e0             	mov    -0x20(%ebp),%eax
f0100bb6:	89 45 d0             	mov    %eax,-0x30(%ebp)
	}
	// Ignore stuff after the colon.
	info->eip_fn_namelen = strfind(info->eip_fn_name, ':') - info->eip_fn_name;
f0100bb9:	83 ec 08             	sub    $0x8,%esp
f0100bbc:	6a 3a                	push   $0x3a
f0100bbe:	ff 73 08             	pushl  0x8(%ebx)
f0100bc1:	e8 56 08 00 00       	call   f010141c <strfind>
f0100bc6:	2b 43 08             	sub    0x8(%ebx),%eax
f0100bc9:	89 43 0c             	mov    %eax,0xc(%ebx)
	// Hint:
	//	There's a particular stabs type used for line numbers.
	//	Look at the STABS documentation and <inc/stab.h> to find
	//	which one.
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
f0100bcc:	83 c4 08             	add    $0x8,%esp
f0100bcf:	56                   	push   %esi
f0100bd0:	6a 44                	push   $0x44
f0100bd2:	8d 4d d0             	lea    -0x30(%ebp),%ecx
f0100bd5:	8d 55 d4             	lea    -0x2c(%ebp),%edx
f0100bd8:	b8 b0 20 10 f0       	mov    $0xf01020b0,%eax
f0100bdd:	e8 b4 fd ff ff       	call   f0100996 <stab_binsearch>
	if (lline <= rline) {
f0100be2:	8b 55 d4             	mov    -0x2c(%ebp),%edx
f0100be5:	83 c4 10             	add    $0x10,%esp
f0100be8:	3b 55 d0             	cmp    -0x30(%ebp),%edx
f0100beb:	0f 8f a7 00 00 00    	jg     f0100c98 <debuginfo_eip+0x20c>
		info->eip_line = stabs[lline].n_desc;
f0100bf1:	8d 04 52             	lea    (%edx,%edx,2),%eax
f0100bf4:	8d 04 85 b0 20 10 f0 	lea    -0xfefdf50(,%eax,4),%eax
f0100bfb:	0f b7 48 06          	movzwl 0x6(%eax),%ecx
f0100bff:	89 4b 04             	mov    %ecx,0x4(%ebx)
	// Search backwards from the line number for the relevant filename
	// stab.
	// We can't just use the "lfile" stab because inlined functions
	// can interpolate code from a different file!
	// Such included source files use the N_SOL stab type.
	while (lline >= lfile
f0100c02:	8b 75 e4             	mov    -0x1c(%ebp),%esi
f0100c05:	eb 06                	jmp    f0100c0d <debuginfo_eip+0x181>
f0100c07:	83 ea 01             	sub    $0x1,%edx
f0100c0a:	83 e8 0c             	sub    $0xc,%eax
f0100c0d:	39 d6                	cmp    %edx,%esi
f0100c0f:	7f 34                	jg     f0100c45 <debuginfo_eip+0x1b9>
	       && stabs[lline].n_type != N_SOL
f0100c11:	0f b6 48 04          	movzbl 0x4(%eax),%ecx
f0100c15:	80 f9 84             	cmp    $0x84,%cl
f0100c18:	74 0b                	je     f0100c25 <debuginfo_eip+0x199>
	       && (stabs[lline].n_type != N_SO || !stabs[lline].n_value))
f0100c1a:	80 f9 64             	cmp    $0x64,%cl
f0100c1d:	75 e8                	jne    f0100c07 <debuginfo_eip+0x17b>
f0100c1f:	83 78 08 00          	cmpl   $0x0,0x8(%eax)
f0100c23:	74 e2                	je     f0100c07 <debuginfo_eip+0x17b>
		lline--;
	if (lline >= lfile && stabs[lline].n_strx < stabstr_end - stabstr)
f0100c25:	8d 04 52             	lea    (%edx,%edx,2),%eax
f0100c28:	8b 14 85 b0 20 10 f0 	mov    -0xfefdf50(,%eax,4),%edx
f0100c2f:	b8 f3 73 10 f0       	mov    $0xf01073f3,%eax
f0100c34:	2d dd 5a 10 f0       	sub    $0xf0105add,%eax
f0100c39:	39 c2                	cmp    %eax,%edx
f0100c3b:	73 08                	jae    f0100c45 <debuginfo_eip+0x1b9>
		info->eip_file = stabstr + stabs[lline].n_strx;
f0100c3d:	81 c2 dd 5a 10 f0    	add    $0xf0105add,%edx
f0100c43:	89 13                	mov    %edx,(%ebx)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f0100c45:	8b 55 dc             	mov    -0x24(%ebp),%edx
f0100c48:	8b 75 d8             	mov    -0x28(%ebp),%esi
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0100c4b:	b8 00 00 00 00       	mov    $0x0,%eax
		info->eip_file = stabstr + stabs[lline].n_strx;


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
f0100c50:	39 f2                	cmp    %esi,%edx
f0100c52:	7d 50                	jge    f0100ca4 <debuginfo_eip+0x218>
		for (lline = lfun + 1;
f0100c54:	83 c2 01             	add    $0x1,%edx
f0100c57:	89 d0                	mov    %edx,%eax
f0100c59:	8d 14 52             	lea    (%edx,%edx,2),%edx
f0100c5c:	8d 14 95 b0 20 10 f0 	lea    -0xfefdf50(,%edx,4),%edx
f0100c63:	eb 04                	jmp    f0100c69 <debuginfo_eip+0x1dd>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;
f0100c65:	83 43 14 01          	addl   $0x1,0x14(%ebx)


	// Set eip_fn_narg to the number of arguments taken by the function,
	// or 0 if there was no containing function.
	if (lfun < rfun)
		for (lline = lfun + 1;
f0100c69:	39 c6                	cmp    %eax,%esi
f0100c6b:	7e 32                	jle    f0100c9f <debuginfo_eip+0x213>
		     lline < rfun && stabs[lline].n_type == N_PSYM;
f0100c6d:	0f b6 4a 04          	movzbl 0x4(%edx),%ecx
f0100c71:	83 c0 01             	add    $0x1,%eax
f0100c74:	83 c2 0c             	add    $0xc,%edx
f0100c77:	80 f9 a0             	cmp    $0xa0,%cl
f0100c7a:	74 e9                	je     f0100c65 <debuginfo_eip+0x1d9>
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0100c7c:	b8 00 00 00 00       	mov    $0x0,%eax
f0100c81:	eb 21                	jmp    f0100ca4 <debuginfo_eip+0x218>
  	        panic("User address");
	}

	// String table validity checks
	if (stabstr_end <= stabstr || stabstr_end[-1] != 0)
		return -1;
f0100c83:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100c88:	eb 1a                	jmp    f0100ca4 <debuginfo_eip+0x218>
f0100c8a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100c8f:	eb 13                	jmp    f0100ca4 <debuginfo_eip+0x218>
	// Search the entire set of stabs for the source file (type N_SO).
	lfile = 0;
	rfile = (stab_end - stabs) - 1;
	stab_binsearch(stabs, &lfile, &rfile, N_SO, addr);
	if (lfile == 0)
		return -1;
f0100c91:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100c96:	eb 0c                	jmp    f0100ca4 <debuginfo_eip+0x218>
	// Your code here.
	stab_binsearch(stabs, &lline, &rline, N_SLINE, addr);
	if (lline <= rline) {
		info->eip_line = stabs[lline].n_desc;
	} else {
		return -1;
f0100c98:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
f0100c9d:	eb 05                	jmp    f0100ca4 <debuginfo_eip+0x218>
		for (lline = lfun + 1;
		     lline < rfun && stabs[lline].n_type == N_PSYM;
		     lline++)
			info->eip_fn_narg++;

	return 0;
f0100c9f:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0100ca4:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100ca7:	5b                   	pop    %ebx
f0100ca8:	5e                   	pop    %esi
f0100ca9:	5f                   	pop    %edi
f0100caa:	5d                   	pop    %ebp
f0100cab:	c3                   	ret    

f0100cac <printnum>:
 * using specified putch function and associated pointer putdat.
 */
static void
printnum(void (*putch)(int, void*), void *putdat,
	 unsigned long long num, unsigned base, int width, int padc)
{
f0100cac:	55                   	push   %ebp
f0100cad:	89 e5                	mov    %esp,%ebp
f0100caf:	57                   	push   %edi
f0100cb0:	56                   	push   %esi
f0100cb1:	53                   	push   %ebx
f0100cb2:	83 ec 1c             	sub    $0x1c,%esp
f0100cb5:	89 c7                	mov    %eax,%edi
f0100cb7:	89 d6                	mov    %edx,%esi
f0100cb9:	8b 45 08             	mov    0x8(%ebp),%eax
f0100cbc:	8b 55 0c             	mov    0xc(%ebp),%edx
f0100cbf:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0100cc2:	89 55 dc             	mov    %edx,-0x24(%ebp)
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
f0100cc5:	8b 4d 10             	mov    0x10(%ebp),%ecx
f0100cc8:	bb 00 00 00 00       	mov    $0x0,%ebx
f0100ccd:	89 4d e0             	mov    %ecx,-0x20(%ebp)
f0100cd0:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
f0100cd3:	39 d3                	cmp    %edx,%ebx
f0100cd5:	72 05                	jb     f0100cdc <printnum+0x30>
f0100cd7:	39 45 10             	cmp    %eax,0x10(%ebp)
f0100cda:	77 45                	ja     f0100d21 <printnum+0x75>
		printnum(putch, putdat, num / base, base, width - 1, padc);
f0100cdc:	83 ec 0c             	sub    $0xc,%esp
f0100cdf:	ff 75 18             	pushl  0x18(%ebp)
f0100ce2:	8b 45 14             	mov    0x14(%ebp),%eax
f0100ce5:	8d 58 ff             	lea    -0x1(%eax),%ebx
f0100ce8:	53                   	push   %ebx
f0100ce9:	ff 75 10             	pushl  0x10(%ebp)
f0100cec:	83 ec 08             	sub    $0x8,%esp
f0100cef:	ff 75 e4             	pushl  -0x1c(%ebp)
f0100cf2:	ff 75 e0             	pushl  -0x20(%ebp)
f0100cf5:	ff 75 dc             	pushl  -0x24(%ebp)
f0100cf8:	ff 75 d8             	pushl  -0x28(%ebp)
f0100cfb:	e8 40 09 00 00       	call   f0101640 <__udivdi3>
f0100d00:	83 c4 18             	add    $0x18,%esp
f0100d03:	52                   	push   %edx
f0100d04:	50                   	push   %eax
f0100d05:	89 f2                	mov    %esi,%edx
f0100d07:	89 f8                	mov    %edi,%eax
f0100d09:	e8 9e ff ff ff       	call   f0100cac <printnum>
f0100d0e:	83 c4 20             	add    $0x20,%esp
f0100d11:	eb 18                	jmp    f0100d2b <printnum+0x7f>
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
			putch(padc, putdat);
f0100d13:	83 ec 08             	sub    $0x8,%esp
f0100d16:	56                   	push   %esi
f0100d17:	ff 75 18             	pushl  0x18(%ebp)
f0100d1a:	ff d7                	call   *%edi
f0100d1c:	83 c4 10             	add    $0x10,%esp
f0100d1f:	eb 03                	jmp    f0100d24 <printnum+0x78>
f0100d21:	8b 5d 14             	mov    0x14(%ebp),%ebx
	// first recursively print all preceding (more significant) digits
	if (num >= base) {
		printnum(putch, putdat, num / base, base, width - 1, padc);
	} else {
		// print any needed pad characters before first digit
		while (--width > 0)
f0100d24:	83 eb 01             	sub    $0x1,%ebx
f0100d27:	85 db                	test   %ebx,%ebx
f0100d29:	7f e8                	jg     f0100d13 <printnum+0x67>
			putch(padc, putdat);
	}

	// then print this (the least significant) digit
	putch("0123456789abcdef"[num % base], putdat);
f0100d2b:	83 ec 08             	sub    $0x8,%esp
f0100d2e:	56                   	push   %esi
f0100d2f:	83 ec 04             	sub    $0x4,%esp
f0100d32:	ff 75 e4             	pushl  -0x1c(%ebp)
f0100d35:	ff 75 e0             	pushl  -0x20(%ebp)
f0100d38:	ff 75 dc             	pushl  -0x24(%ebp)
f0100d3b:	ff 75 d8             	pushl  -0x28(%ebp)
f0100d3e:	e8 2d 0a 00 00       	call   f0101770 <__umoddi3>
f0100d43:	83 c4 14             	add    $0x14,%esp
f0100d46:	0f be 80 89 1e 10 f0 	movsbl -0xfefe177(%eax),%eax
f0100d4d:	50                   	push   %eax
f0100d4e:	ff d7                	call   *%edi
}
f0100d50:	83 c4 10             	add    $0x10,%esp
f0100d53:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0100d56:	5b                   	pop    %ebx
f0100d57:	5e                   	pop    %esi
f0100d58:	5f                   	pop    %edi
f0100d59:	5d                   	pop    %ebp
f0100d5a:	c3                   	ret    

f0100d5b <getuint>:

// Get an unsigned int of various possible sizes from a varargs list,
// depending on the lflag parameter.
static unsigned long long
getuint(va_list *ap, int lflag)
{
f0100d5b:	55                   	push   %ebp
f0100d5c:	89 e5                	mov    %esp,%ebp
	if (lflag >= 2)
f0100d5e:	83 fa 01             	cmp    $0x1,%edx
f0100d61:	7e 0e                	jle    f0100d71 <getuint+0x16>
		return va_arg(*ap, unsigned long long);
f0100d63:	8b 10                	mov    (%eax),%edx
f0100d65:	8d 4a 08             	lea    0x8(%edx),%ecx
f0100d68:	89 08                	mov    %ecx,(%eax)
f0100d6a:	8b 02                	mov    (%edx),%eax
f0100d6c:	8b 52 04             	mov    0x4(%edx),%edx
f0100d6f:	eb 22                	jmp    f0100d93 <getuint+0x38>
	else if (lflag)
f0100d71:	85 d2                	test   %edx,%edx
f0100d73:	74 10                	je     f0100d85 <getuint+0x2a>
		return va_arg(*ap, unsigned long);
f0100d75:	8b 10                	mov    (%eax),%edx
f0100d77:	8d 4a 04             	lea    0x4(%edx),%ecx
f0100d7a:	89 08                	mov    %ecx,(%eax)
f0100d7c:	8b 02                	mov    (%edx),%eax
f0100d7e:	ba 00 00 00 00       	mov    $0x0,%edx
f0100d83:	eb 0e                	jmp    f0100d93 <getuint+0x38>
	else
		return va_arg(*ap, unsigned int);
f0100d85:	8b 10                	mov    (%eax),%edx
f0100d87:	8d 4a 04             	lea    0x4(%edx),%ecx
f0100d8a:	89 08                	mov    %ecx,(%eax)
f0100d8c:	8b 02                	mov    (%edx),%eax
f0100d8e:	ba 00 00 00 00       	mov    $0x0,%edx
}
f0100d93:	5d                   	pop    %ebp
f0100d94:	c3                   	ret    

f0100d95 <sprintputch>:
	int cnt;
};

static void
sprintputch(int ch, struct sprintbuf *b)
{
f0100d95:	55                   	push   %ebp
f0100d96:	89 e5                	mov    %esp,%ebp
f0100d98:	8b 45 0c             	mov    0xc(%ebp),%eax
	b->cnt++;
f0100d9b:	83 40 08 01          	addl   $0x1,0x8(%eax)
	if (b->buf < b->ebuf)
f0100d9f:	8b 10                	mov    (%eax),%edx
f0100da1:	3b 50 04             	cmp    0x4(%eax),%edx
f0100da4:	73 0a                	jae    f0100db0 <sprintputch+0x1b>
		*b->buf++ = ch;
f0100da6:	8d 4a 01             	lea    0x1(%edx),%ecx
f0100da9:	89 08                	mov    %ecx,(%eax)
f0100dab:	8b 45 08             	mov    0x8(%ebp),%eax
f0100dae:	88 02                	mov    %al,(%edx)
}
f0100db0:	5d                   	pop    %ebp
f0100db1:	c3                   	ret    

f0100db2 <printfmt>:
	}
}

void
printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...)
{
f0100db2:	55                   	push   %ebp
f0100db3:	89 e5                	mov    %esp,%ebp
f0100db5:	83 ec 08             	sub    $0x8,%esp
	va_list ap;

	va_start(ap, fmt);
f0100db8:	8d 45 14             	lea    0x14(%ebp),%eax
	vprintfmt(putch, putdat, fmt, ap);
f0100dbb:	50                   	push   %eax
f0100dbc:	ff 75 10             	pushl  0x10(%ebp)
f0100dbf:	ff 75 0c             	pushl  0xc(%ebp)
f0100dc2:	ff 75 08             	pushl  0x8(%ebp)
f0100dc5:	e8 05 00 00 00       	call   f0100dcf <vprintfmt>
	va_end(ap);
}
f0100dca:	83 c4 10             	add    $0x10,%esp
f0100dcd:	c9                   	leave  
f0100dce:	c3                   	ret    

f0100dcf <vprintfmt>:
// Main function to format and print a string.
void printfmt(void (*putch)(int, void*), void *putdat, const char *fmt, ...);

void
vprintfmt(void (*putch)(int, void*), void *putdat, const char *fmt, va_list ap)
{
f0100dcf:	55                   	push   %ebp
f0100dd0:	89 e5                	mov    %esp,%ebp
f0100dd2:	57                   	push   %edi
f0100dd3:	56                   	push   %esi
f0100dd4:	53                   	push   %ebx
f0100dd5:	83 ec 2c             	sub    $0x2c,%esp
f0100dd8:	8b 75 08             	mov    0x8(%ebp),%esi
f0100ddb:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0100dde:	8b 7d 10             	mov    0x10(%ebp),%edi
f0100de1:	eb 12                	jmp    f0100df5 <vprintfmt+0x26>
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
			if (ch == '\0')
f0100de3:	85 c0                	test   %eax,%eax
f0100de5:	0f 84 86 03 00 00    	je     f0101171 <vprintfmt+0x3a2>
				return;
			putch(ch, putdat);
f0100deb:	83 ec 08             	sub    $0x8,%esp
f0100dee:	53                   	push   %ebx
f0100def:	50                   	push   %eax
f0100df0:	ff d6                	call   *%esi
f0100df2:	83 c4 10             	add    $0x10,%esp
	unsigned long long num;
	int base, lflag, width, precision, altflag;
	char padc;

	while (1) {
		while ((ch = *(unsigned char *) fmt++) != '%') {
f0100df5:	83 c7 01             	add    $0x1,%edi
f0100df8:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f0100dfc:	83 f8 25             	cmp    $0x25,%eax
f0100dff:	75 e2                	jne    f0100de3 <vprintfmt+0x14>
f0100e01:	c6 45 d4 20          	movb   $0x20,-0x2c(%ebp)
f0100e05:	c7 45 d8 00 00 00 00 	movl   $0x0,-0x28(%ebp)
f0100e0c:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0100e13:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
f0100e1a:	ba 00 00 00 00       	mov    $0x0,%edx
f0100e1f:	eb 07                	jmp    f0100e28 <vprintfmt+0x59>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100e21:	8b 7d e4             	mov    -0x1c(%ebp),%edi

		// flag to pad on the right
		case '-':
			padc = '-';
f0100e24:	c6 45 d4 2d          	movb   $0x2d,-0x2c(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100e28:	8d 47 01             	lea    0x1(%edi),%eax
f0100e2b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
f0100e2e:	0f b6 07             	movzbl (%edi),%eax
f0100e31:	0f b6 c8             	movzbl %al,%ecx
f0100e34:	83 e8 23             	sub    $0x23,%eax
f0100e37:	3c 55                	cmp    $0x55,%al
f0100e39:	0f 87 17 03 00 00    	ja     f0101156 <vprintfmt+0x387>
f0100e3f:	0f b6 c0             	movzbl %al,%eax
f0100e42:	ff 24 85 20 1f 10 f0 	jmp    *-0xfefe0e0(,%eax,4)
f0100e49:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			padc = '-';
			goto reswitch;

		// flag to pad with 0's instead of spaces
		case '0':
			padc = '0';
f0100e4c:	c6 45 d4 30          	movb   $0x30,-0x2c(%ebp)
f0100e50:	eb d6                	jmp    f0100e28 <vprintfmt+0x59>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100e52:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0100e55:	b8 00 00 00 00       	mov    $0x0,%eax
f0100e5a:	89 55 e4             	mov    %edx,-0x1c(%ebp)
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
				precision = precision * 10 + ch - '0';
f0100e5d:	8d 04 80             	lea    (%eax,%eax,4),%eax
f0100e60:	8d 44 41 d0          	lea    -0x30(%ecx,%eax,2),%eax
				ch = *fmt;
f0100e64:	0f be 0f             	movsbl (%edi),%ecx
				if (ch < '0' || ch > '9')
f0100e67:	8d 51 d0             	lea    -0x30(%ecx),%edx
f0100e6a:	83 fa 09             	cmp    $0x9,%edx
f0100e6d:	77 38                	ja     f0100ea7 <vprintfmt+0xd8>
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			for (precision = 0; ; ++fmt) {
f0100e6f:	83 c7 01             	add    $0x1,%edi
				precision = precision * 10 + ch - '0';
				ch = *fmt;
				if (ch < '0' || ch > '9')
					break;
			}
f0100e72:	eb e9                	jmp    f0100e5d <vprintfmt+0x8e>
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
f0100e74:	8b 45 14             	mov    0x14(%ebp),%eax
f0100e77:	8d 48 04             	lea    0x4(%eax),%ecx
f0100e7a:	89 4d 14             	mov    %ecx,0x14(%ebp)
f0100e7d:	8b 00                	mov    (%eax),%eax
f0100e7f:	89 45 d0             	mov    %eax,-0x30(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100e82:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			}
			goto process_precision;

		case '*':
			precision = va_arg(ap, int);
			goto process_precision;
f0100e85:	eb 26                	jmp    f0100ead <vprintfmt+0xde>
f0100e87:	8b 4d e0             	mov    -0x20(%ebp),%ecx
f0100e8a:	89 c8                	mov    %ecx,%eax
f0100e8c:	c1 f8 1f             	sar    $0x1f,%eax
f0100e8f:	f7 d0                	not    %eax
f0100e91:	21 c1                	and    %eax,%ecx
f0100e93:	89 4d e0             	mov    %ecx,-0x20(%ebp)
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100e96:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0100e99:	eb 8d                	jmp    f0100e28 <vprintfmt+0x59>
f0100e9b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			if (width < 0)
				width = 0;
			goto reswitch;

		case '#':
			altflag = 1;
f0100e9e:	c7 45 d8 01 00 00 00 	movl   $0x1,-0x28(%ebp)
			goto reswitch;
f0100ea5:	eb 81                	jmp    f0100e28 <vprintfmt+0x59>
f0100ea7:	8b 55 e4             	mov    -0x1c(%ebp),%edx
f0100eaa:	89 45 d0             	mov    %eax,-0x30(%ebp)

		process_precision:
			if (width < 0)
f0100ead:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f0100eb1:	0f 89 71 ff ff ff    	jns    f0100e28 <vprintfmt+0x59>
				width = precision, precision = -1;
f0100eb7:	8b 45 d0             	mov    -0x30(%ebp),%eax
f0100eba:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0100ebd:	c7 45 d0 ff ff ff ff 	movl   $0xffffffff,-0x30(%ebp)
f0100ec4:	e9 5f ff ff ff       	jmp    f0100e28 <vprintfmt+0x59>
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
f0100ec9:	83 c2 01             	add    $0x1,%edx
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100ecc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			goto reswitch;

		// long flag (doubled for long long)
		case 'l':
			lflag++;
			goto reswitch;
f0100ecf:	e9 54 ff ff ff       	jmp    f0100e28 <vprintfmt+0x59>

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
f0100ed4:	8b 45 14             	mov    0x14(%ebp),%eax
f0100ed7:	8d 50 04             	lea    0x4(%eax),%edx
f0100eda:	89 55 14             	mov    %edx,0x14(%ebp)
f0100edd:	83 ec 08             	sub    $0x8,%esp
f0100ee0:	53                   	push   %ebx
f0100ee1:	ff 30                	pushl  (%eax)
f0100ee3:	ff d6                	call   *%esi
			break;
f0100ee5:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100ee8:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			goto reswitch;

		// character
		case 'c':
			putch(va_arg(ap, int), putdat);
			break;
f0100eeb:	e9 05 ff ff ff       	jmp    f0100df5 <vprintfmt+0x26>

		// error message
		case 'e':
			err = va_arg(ap, int);
f0100ef0:	8b 45 14             	mov    0x14(%ebp),%eax
f0100ef3:	8d 50 04             	lea    0x4(%eax),%edx
f0100ef6:	89 55 14             	mov    %edx,0x14(%ebp)
f0100ef9:	8b 00                	mov    (%eax),%eax
f0100efb:	99                   	cltd   
f0100efc:	31 d0                	xor    %edx,%eax
f0100efe:	29 d0                	sub    %edx,%eax
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
f0100f00:	83 f8 07             	cmp    $0x7,%eax
f0100f03:	7f 0b                	jg     f0100f10 <vprintfmt+0x141>
f0100f05:	8b 14 85 80 20 10 f0 	mov    -0xfefdf80(,%eax,4),%edx
f0100f0c:	85 d2                	test   %edx,%edx
f0100f0e:	75 18                	jne    f0100f28 <vprintfmt+0x159>
				printfmt(putch, putdat, "error %d", err);
f0100f10:	50                   	push   %eax
f0100f11:	68 a1 1e 10 f0       	push   $0xf0101ea1
f0100f16:	53                   	push   %ebx
f0100f17:	56                   	push   %esi
f0100f18:	e8 95 fe ff ff       	call   f0100db2 <printfmt>
f0100f1d:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100f20:	8b 7d e4             	mov    -0x1c(%ebp),%edi
		case 'e':
			err = va_arg(ap, int);
			if (err < 0)
				err = -err;
			if (err >= MAXERROR || (p = error_string[err]) == NULL)
				printfmt(putch, putdat, "error %d", err);
f0100f23:	e9 cd fe ff ff       	jmp    f0100df5 <vprintfmt+0x26>
			else
				printfmt(putch, putdat, "%s", p);
f0100f28:	52                   	push   %edx
f0100f29:	68 aa 1e 10 f0       	push   $0xf0101eaa
f0100f2e:	53                   	push   %ebx
f0100f2f:	56                   	push   %esi
f0100f30:	e8 7d fe ff ff       	call   f0100db2 <printfmt>
f0100f35:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0100f38:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f0100f3b:	e9 b5 fe ff ff       	jmp    f0100df5 <vprintfmt+0x26>
				printfmt(putch, putdat, "%s", p);
			break;

		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
f0100f40:	8b 45 14             	mov    0x14(%ebp),%eax
f0100f43:	8d 50 04             	lea    0x4(%eax),%edx
f0100f46:	89 55 14             	mov    %edx,0x14(%ebp)
f0100f49:	8b 38                	mov    (%eax),%edi
f0100f4b:	85 ff                	test   %edi,%edi
f0100f4d:	75 05                	jne    f0100f54 <vprintfmt+0x185>
				p = "(null)";
f0100f4f:	bf 9a 1e 10 f0       	mov    $0xf0101e9a,%edi
			if (width > 0 && padc != '-')
f0100f54:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
f0100f58:	0f 8e 93 00 00 00    	jle    f0100ff1 <vprintfmt+0x222>
f0100f5e:	80 7d d4 2d          	cmpb   $0x2d,-0x2c(%ebp)
f0100f62:	0f 84 97 00 00 00    	je     f0100fff <vprintfmt+0x230>
				for (width -= strnlen(p, precision); width > 0; width--)
f0100f68:	83 ec 08             	sub    $0x8,%esp
f0100f6b:	ff 75 d0             	pushl  -0x30(%ebp)
f0100f6e:	57                   	push   %edi
f0100f6f:	e8 5e 03 00 00       	call   f01012d2 <strnlen>
f0100f74:	8b 4d e0             	mov    -0x20(%ebp),%ecx
f0100f77:	29 c1                	sub    %eax,%ecx
f0100f79:	89 4d cc             	mov    %ecx,-0x34(%ebp)
f0100f7c:	83 c4 10             	add    $0x10,%esp
					putch(padc, putdat);
f0100f7f:	0f be 45 d4          	movsbl -0x2c(%ebp),%eax
f0100f83:	89 45 e0             	mov    %eax,-0x20(%ebp)
f0100f86:	89 7d d4             	mov    %edi,-0x2c(%ebp)
f0100f89:	89 cf                	mov    %ecx,%edi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0100f8b:	eb 0f                	jmp    f0100f9c <vprintfmt+0x1cd>
					putch(padc, putdat);
f0100f8d:	83 ec 08             	sub    $0x8,%esp
f0100f90:	53                   	push   %ebx
f0100f91:	ff 75 e0             	pushl  -0x20(%ebp)
f0100f94:	ff d6                	call   *%esi
		// string
		case 's':
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
f0100f96:	83 ef 01             	sub    $0x1,%edi
f0100f99:	83 c4 10             	add    $0x10,%esp
f0100f9c:	85 ff                	test   %edi,%edi
f0100f9e:	7f ed                	jg     f0100f8d <vprintfmt+0x1be>
f0100fa0:	8b 7d d4             	mov    -0x2c(%ebp),%edi
f0100fa3:	8b 4d cc             	mov    -0x34(%ebp),%ecx
f0100fa6:	89 c8                	mov    %ecx,%eax
f0100fa8:	c1 f8 1f             	sar    $0x1f,%eax
f0100fab:	f7 d0                	not    %eax
f0100fad:	21 c8                	and    %ecx,%eax
f0100faf:	29 c1                	sub    %eax,%ecx
f0100fb1:	89 75 08             	mov    %esi,0x8(%ebp)
f0100fb4:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0100fb7:	89 5d 0c             	mov    %ebx,0xc(%ebp)
f0100fba:	89 cb                	mov    %ecx,%ebx
f0100fbc:	eb 4d                	jmp    f010100b <vprintfmt+0x23c>
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
f0100fbe:	83 7d d8 00          	cmpl   $0x0,-0x28(%ebp)
f0100fc2:	74 1b                	je     f0100fdf <vprintfmt+0x210>
f0100fc4:	0f be c0             	movsbl %al,%eax
f0100fc7:	83 e8 20             	sub    $0x20,%eax
f0100fca:	83 f8 5e             	cmp    $0x5e,%eax
f0100fcd:	76 10                	jbe    f0100fdf <vprintfmt+0x210>
					putch('?', putdat);
f0100fcf:	83 ec 08             	sub    $0x8,%esp
f0100fd2:	ff 75 0c             	pushl  0xc(%ebp)
f0100fd5:	6a 3f                	push   $0x3f
f0100fd7:	ff 55 08             	call   *0x8(%ebp)
f0100fda:	83 c4 10             	add    $0x10,%esp
f0100fdd:	eb 0d                	jmp    f0100fec <vprintfmt+0x21d>
				else
					putch(ch, putdat);
f0100fdf:	83 ec 08             	sub    $0x8,%esp
f0100fe2:	ff 75 0c             	pushl  0xc(%ebp)
f0100fe5:	52                   	push   %edx
f0100fe6:	ff 55 08             	call   *0x8(%ebp)
f0100fe9:	83 c4 10             	add    $0x10,%esp
			if ((p = va_arg(ap, char *)) == NULL)
				p = "(null)";
			if (width > 0 && padc != '-')
				for (width -= strnlen(p, precision); width > 0; width--)
					putch(padc, putdat);
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
f0100fec:	83 eb 01             	sub    $0x1,%ebx
f0100fef:	eb 1a                	jmp    f010100b <vprintfmt+0x23c>
f0100ff1:	89 75 08             	mov    %esi,0x8(%ebp)
f0100ff4:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0100ff7:	89 5d 0c             	mov    %ebx,0xc(%ebp)
f0100ffa:	8b 5d e0             	mov    -0x20(%ebp),%ebx
f0100ffd:	eb 0c                	jmp    f010100b <vprintfmt+0x23c>
f0100fff:	89 75 08             	mov    %esi,0x8(%ebp)
f0101002:	8b 75 d0             	mov    -0x30(%ebp),%esi
f0101005:	89 5d 0c             	mov    %ebx,0xc(%ebp)
f0101008:	8b 5d e0             	mov    -0x20(%ebp),%ebx
f010100b:	83 c7 01             	add    $0x1,%edi
f010100e:	0f b6 47 ff          	movzbl -0x1(%edi),%eax
f0101012:	0f be d0             	movsbl %al,%edx
f0101015:	85 d2                	test   %edx,%edx
f0101017:	74 23                	je     f010103c <vprintfmt+0x26d>
f0101019:	85 f6                	test   %esi,%esi
f010101b:	78 a1                	js     f0100fbe <vprintfmt+0x1ef>
f010101d:	83 ee 01             	sub    $0x1,%esi
f0101020:	79 9c                	jns    f0100fbe <vprintfmt+0x1ef>
f0101022:	89 df                	mov    %ebx,%edi
f0101024:	8b 75 08             	mov    0x8(%ebp),%esi
f0101027:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f010102a:	eb 18                	jmp    f0101044 <vprintfmt+0x275>
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
				putch(' ', putdat);
f010102c:	83 ec 08             	sub    $0x8,%esp
f010102f:	53                   	push   %ebx
f0101030:	6a 20                	push   $0x20
f0101032:	ff d6                	call   *%esi
			for (; (ch = *p++) != '\0' && (precision < 0 || --precision >= 0); width--)
				if (altflag && (ch < ' ' || ch > '~'))
					putch('?', putdat);
				else
					putch(ch, putdat);
			for (; width > 0; width--)
f0101034:	83 ef 01             	sub    $0x1,%edi
f0101037:	83 c4 10             	add    $0x10,%esp
f010103a:	eb 08                	jmp    f0101044 <vprintfmt+0x275>
f010103c:	89 df                	mov    %ebx,%edi
f010103e:	8b 75 08             	mov    0x8(%ebp),%esi
f0101041:	8b 5d 0c             	mov    0xc(%ebp),%ebx
f0101044:	85 ff                	test   %edi,%edi
f0101046:	7f e4                	jg     f010102c <vprintfmt+0x25d>
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f0101048:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f010104b:	e9 a5 fd ff ff       	jmp    f0100df5 <vprintfmt+0x26>
// Same as getuint but signed - can't use getuint
// because of sign extension
static long long
getint(va_list *ap, int lflag)
{
	if (lflag >= 2)
f0101050:	83 fa 01             	cmp    $0x1,%edx
f0101053:	7e 16                	jle    f010106b <vprintfmt+0x29c>
		return va_arg(*ap, long long);
f0101055:	8b 45 14             	mov    0x14(%ebp),%eax
f0101058:	8d 50 08             	lea    0x8(%eax),%edx
f010105b:	89 55 14             	mov    %edx,0x14(%ebp)
f010105e:	8b 50 04             	mov    0x4(%eax),%edx
f0101061:	8b 00                	mov    (%eax),%eax
f0101063:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0101066:	89 55 dc             	mov    %edx,-0x24(%ebp)
f0101069:	eb 32                	jmp    f010109d <vprintfmt+0x2ce>
	else if (lflag)
f010106b:	85 d2                	test   %edx,%edx
f010106d:	74 18                	je     f0101087 <vprintfmt+0x2b8>
		return va_arg(*ap, long);
f010106f:	8b 45 14             	mov    0x14(%ebp),%eax
f0101072:	8d 50 04             	lea    0x4(%eax),%edx
f0101075:	89 55 14             	mov    %edx,0x14(%ebp)
f0101078:	8b 00                	mov    (%eax),%eax
f010107a:	89 45 d8             	mov    %eax,-0x28(%ebp)
f010107d:	89 c1                	mov    %eax,%ecx
f010107f:	c1 f9 1f             	sar    $0x1f,%ecx
f0101082:	89 4d dc             	mov    %ecx,-0x24(%ebp)
f0101085:	eb 16                	jmp    f010109d <vprintfmt+0x2ce>
	else
		return va_arg(*ap, int);
f0101087:	8b 45 14             	mov    0x14(%ebp),%eax
f010108a:	8d 50 04             	lea    0x4(%eax),%edx
f010108d:	89 55 14             	mov    %edx,0x14(%ebp)
f0101090:	8b 00                	mov    (%eax),%eax
f0101092:	89 45 d8             	mov    %eax,-0x28(%ebp)
f0101095:	89 c1                	mov    %eax,%ecx
f0101097:	c1 f9 1f             	sar    $0x1f,%ecx
f010109a:	89 4d dc             	mov    %ecx,-0x24(%ebp)
				putch(' ', putdat);
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
f010109d:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01010a0:	8b 55 dc             	mov    -0x24(%ebp),%edx
			if ((long long) num < 0) {
				putch('-', putdat);
				num = -(long long) num;
			}
			base = 10;
f01010a3:	b9 0a 00 00 00       	mov    $0xa,%ecx
			break;

		// (signed) decimal
		case 'd':
			num = getint(&ap, lflag);
			if ((long long) num < 0) {
f01010a8:	83 7d dc 00          	cmpl   $0x0,-0x24(%ebp)
f01010ac:	79 74                	jns    f0101122 <vprintfmt+0x353>
				putch('-', putdat);
f01010ae:	83 ec 08             	sub    $0x8,%esp
f01010b1:	53                   	push   %ebx
f01010b2:	6a 2d                	push   $0x2d
f01010b4:	ff d6                	call   *%esi
				num = -(long long) num;
f01010b6:	8b 45 d8             	mov    -0x28(%ebp),%eax
f01010b9:	8b 55 dc             	mov    -0x24(%ebp),%edx
f01010bc:	f7 d8                	neg    %eax
f01010be:	83 d2 00             	adc    $0x0,%edx
f01010c1:	f7 da                	neg    %edx
f01010c3:	83 c4 10             	add    $0x10,%esp
			}
			base = 10;
f01010c6:	b9 0a 00 00 00       	mov    $0xa,%ecx
f01010cb:	eb 55                	jmp    f0101122 <vprintfmt+0x353>
			goto number;

		// unsigned decimal
		case 'u':
			num = getuint(&ap, lflag);
f01010cd:	8d 45 14             	lea    0x14(%ebp),%eax
f01010d0:	e8 86 fc ff ff       	call   f0100d5b <getuint>
			base = 10;
f01010d5:	b9 0a 00 00 00       	mov    $0xa,%ecx
			goto number;
f01010da:	eb 46                	jmp    f0101122 <vprintfmt+0x353>
			// Replace this with your code.
			// putch('X', putdat);
			// putch('X', putdat);
			// putch('X', putdat);
			// break;
			num = getuint(&ap, lflag);
f01010dc:	8d 45 14             	lea    0x14(%ebp),%eax
f01010df:	e8 77 fc ff ff       	call   f0100d5b <getuint>
			base = 8;
f01010e4:	b9 08 00 00 00       	mov    $0x8,%ecx
			goto number;
f01010e9:	eb 37                	jmp    f0101122 <vprintfmt+0x353>

		// pointer
		case 'p':
			putch('0', putdat);
f01010eb:	83 ec 08             	sub    $0x8,%esp
f01010ee:	53                   	push   %ebx
f01010ef:	6a 30                	push   $0x30
f01010f1:	ff d6                	call   *%esi
			putch('x', putdat);
f01010f3:	83 c4 08             	add    $0x8,%esp
f01010f6:	53                   	push   %ebx
f01010f7:	6a 78                	push   $0x78
f01010f9:	ff d6                	call   *%esi
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
f01010fb:	8b 45 14             	mov    0x14(%ebp),%eax
f01010fe:	8d 50 04             	lea    0x4(%eax),%edx
f0101101:	89 55 14             	mov    %edx,0x14(%ebp)

		// pointer
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
f0101104:	8b 00                	mov    (%eax),%eax
f0101106:	ba 00 00 00 00       	mov    $0x0,%edx
				(uintptr_t) va_arg(ap, void *);
			base = 16;
			goto number;
f010110b:	83 c4 10             	add    $0x10,%esp
		case 'p':
			putch('0', putdat);
			putch('x', putdat);
			num = (unsigned long long)
				(uintptr_t) va_arg(ap, void *);
			base = 16;
f010110e:	b9 10 00 00 00       	mov    $0x10,%ecx
			goto number;
f0101113:	eb 0d                	jmp    f0101122 <vprintfmt+0x353>

		// (unsigned) hexadecimal
		case 'x':
			num = getuint(&ap, lflag);
f0101115:	8d 45 14             	lea    0x14(%ebp),%eax
f0101118:	e8 3e fc ff ff       	call   f0100d5b <getuint>
			base = 16;
f010111d:	b9 10 00 00 00       	mov    $0x10,%ecx
		number:
			printnum(putch, putdat, num, base, width, padc);
f0101122:	83 ec 0c             	sub    $0xc,%esp
f0101125:	0f be 7d d4          	movsbl -0x2c(%ebp),%edi
f0101129:	57                   	push   %edi
f010112a:	ff 75 e0             	pushl  -0x20(%ebp)
f010112d:	51                   	push   %ecx
f010112e:	52                   	push   %edx
f010112f:	50                   	push   %eax
f0101130:	89 da                	mov    %ebx,%edx
f0101132:	89 f0                	mov    %esi,%eax
f0101134:	e8 73 fb ff ff       	call   f0100cac <printnum>
			break;
f0101139:	83 c4 20             	add    $0x20,%esp
f010113c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
f010113f:	e9 b1 fc ff ff       	jmp    f0100df5 <vprintfmt+0x26>

		// escaped '%' character
		case '%':
			putch(ch, putdat);
f0101144:	83 ec 08             	sub    $0x8,%esp
f0101147:	53                   	push   %ebx
f0101148:	51                   	push   %ecx
f0101149:	ff d6                	call   *%esi
			break;
f010114b:	83 c4 10             	add    $0x10,%esp
		width = -1;
		precision = -1;
		lflag = 0;
		altflag = 0;
	reswitch:
		switch (ch = *(unsigned char *) fmt++) {
f010114e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
			break;

		// escaped '%' character
		case '%':
			putch(ch, putdat);
			break;
f0101151:	e9 9f fc ff ff       	jmp    f0100df5 <vprintfmt+0x26>

		// unrecognized escape sequence - just print it literally
		default:
			putch('%', putdat);
f0101156:	83 ec 08             	sub    $0x8,%esp
f0101159:	53                   	push   %ebx
f010115a:	6a 25                	push   $0x25
f010115c:	ff d6                	call   *%esi
			for (fmt--; fmt[-1] != '%'; fmt--)
f010115e:	83 c4 10             	add    $0x10,%esp
f0101161:	eb 03                	jmp    f0101166 <vprintfmt+0x397>
f0101163:	83 ef 01             	sub    $0x1,%edi
f0101166:	80 7f ff 25          	cmpb   $0x25,-0x1(%edi)
f010116a:	75 f7                	jne    f0101163 <vprintfmt+0x394>
f010116c:	e9 84 fc ff ff       	jmp    f0100df5 <vprintfmt+0x26>
				/* do nothing */;
			break;
		}
	}
}
f0101171:	8d 65 f4             	lea    -0xc(%ebp),%esp
f0101174:	5b                   	pop    %ebx
f0101175:	5e                   	pop    %esi
f0101176:	5f                   	pop    %edi
f0101177:	5d                   	pop    %ebp
f0101178:	c3                   	ret    

f0101179 <vsnprintf>:
		*b->buf++ = ch;
}

int
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
f0101179:	55                   	push   %ebp
f010117a:	89 e5                	mov    %esp,%ebp
f010117c:	83 ec 18             	sub    $0x18,%esp
f010117f:	8b 45 08             	mov    0x8(%ebp),%eax
f0101182:	8b 55 0c             	mov    0xc(%ebp),%edx
	struct sprintbuf b = {buf, buf+n-1, 0};
f0101185:	89 45 ec             	mov    %eax,-0x14(%ebp)
f0101188:	8d 4c 10 ff          	lea    -0x1(%eax,%edx,1),%ecx
f010118c:	89 4d f0             	mov    %ecx,-0x10(%ebp)
f010118f:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%ebp)

	if (buf == NULL || n < 1)
f0101196:	85 c0                	test   %eax,%eax
f0101198:	74 26                	je     f01011c0 <vsnprintf+0x47>
f010119a:	85 d2                	test   %edx,%edx
f010119c:	7e 22                	jle    f01011c0 <vsnprintf+0x47>
		return -E_INVAL;

	// print the string to the buffer
	vprintfmt((void*)sprintputch, &b, fmt, ap);
f010119e:	ff 75 14             	pushl  0x14(%ebp)
f01011a1:	ff 75 10             	pushl  0x10(%ebp)
f01011a4:	8d 45 ec             	lea    -0x14(%ebp),%eax
f01011a7:	50                   	push   %eax
f01011a8:	68 95 0d 10 f0       	push   $0xf0100d95
f01011ad:	e8 1d fc ff ff       	call   f0100dcf <vprintfmt>

	// null terminate the buffer
	*b.buf = '\0';
f01011b2:	8b 45 ec             	mov    -0x14(%ebp),%eax
f01011b5:	c6 00 00             	movb   $0x0,(%eax)

	return b.cnt;
f01011b8:	8b 45 f4             	mov    -0xc(%ebp),%eax
f01011bb:	83 c4 10             	add    $0x10,%esp
f01011be:	eb 05                	jmp    f01011c5 <vsnprintf+0x4c>
vsnprintf(char *buf, int n, const char *fmt, va_list ap)
{
	struct sprintbuf b = {buf, buf+n-1, 0};

	if (buf == NULL || n < 1)
		return -E_INVAL;
f01011c0:	b8 fd ff ff ff       	mov    $0xfffffffd,%eax

	// null terminate the buffer
	*b.buf = '\0';

	return b.cnt;
}
f01011c5:	c9                   	leave  
f01011c6:	c3                   	ret    

f01011c7 <snprintf>:

int
snprintf(char *buf, int n, const char *fmt, ...)
{
f01011c7:	55                   	push   %ebp
f01011c8:	89 e5                	mov    %esp,%ebp
f01011ca:	83 ec 08             	sub    $0x8,%esp
	va_list ap;
	int rc;

	va_start(ap, fmt);
f01011cd:	8d 45 14             	lea    0x14(%ebp),%eax
	rc = vsnprintf(buf, n, fmt, ap);
f01011d0:	50                   	push   %eax
f01011d1:	ff 75 10             	pushl  0x10(%ebp)
f01011d4:	ff 75 0c             	pushl  0xc(%ebp)
f01011d7:	ff 75 08             	pushl  0x8(%ebp)
f01011da:	e8 9a ff ff ff       	call   f0101179 <vsnprintf>
	va_end(ap);

	return rc;
}
f01011df:	c9                   	leave  
f01011e0:	c3                   	ret    

f01011e1 <readline>:
#define BUFLEN 1024
static char buf[BUFLEN];

char *
readline(const char *prompt)
{
f01011e1:	55                   	push   %ebp
f01011e2:	89 e5                	mov    %esp,%ebp
f01011e4:	57                   	push   %edi
f01011e5:	56                   	push   %esi
f01011e6:	53                   	push   %ebx
f01011e7:	83 ec 0c             	sub    $0xc,%esp
f01011ea:	8b 45 08             	mov    0x8(%ebp),%eax
	int i, c, echoing;

	if (prompt != NULL)
f01011ed:	85 c0                	test   %eax,%eax
f01011ef:	74 11                	je     f0101202 <readline+0x21>
		cprintf("%s", prompt);
f01011f1:	83 ec 08             	sub    $0x8,%esp
f01011f4:	50                   	push   %eax
f01011f5:	68 aa 1e 10 f0       	push   $0xf0101eaa
f01011fa:	e8 83 f7 ff ff       	call   f0100982 <cprintf>
f01011ff:	83 c4 10             	add    $0x10,%esp

	i = 0;
	echoing = iscons(0);
f0101202:	83 ec 0c             	sub    $0xc,%esp
f0101205:	6a 00                	push   $0x0
f0101207:	e8 49 f4 ff ff       	call   f0100655 <iscons>
f010120c:	89 c7                	mov    %eax,%edi
f010120e:	83 c4 10             	add    $0x10,%esp
	int i, c, echoing;

	if (prompt != NULL)
		cprintf("%s", prompt);

	i = 0;
f0101211:	be 00 00 00 00       	mov    $0x0,%esi
	echoing = iscons(0);
	while (1) {
		c = getchar();
f0101216:	e8 29 f4 ff ff       	call   f0100644 <getchar>
f010121b:	89 c3                	mov    %eax,%ebx
		if (c < 0) {
f010121d:	85 c0                	test   %eax,%eax
f010121f:	79 18                	jns    f0101239 <readline+0x58>
			cprintf("read error: %e\n", c);
f0101221:	83 ec 08             	sub    $0x8,%esp
f0101224:	50                   	push   %eax
f0101225:	68 a0 20 10 f0       	push   $0xf01020a0
f010122a:	e8 53 f7 ff ff       	call   f0100982 <cprintf>
			return NULL;
f010122f:	83 c4 10             	add    $0x10,%esp
f0101232:	b8 00 00 00 00       	mov    $0x0,%eax
f0101237:	eb 79                	jmp    f01012b2 <readline+0xd1>
		} else if ((c == '\b' || c == '\x7f') && i > 0) {
f0101239:	83 f8 08             	cmp    $0x8,%eax
f010123c:	0f 94 c2             	sete   %dl
f010123f:	83 f8 7f             	cmp    $0x7f,%eax
f0101242:	0f 94 c0             	sete   %al
f0101245:	08 c2                	or     %al,%dl
f0101247:	74 1a                	je     f0101263 <readline+0x82>
f0101249:	85 f6                	test   %esi,%esi
f010124b:	7e 16                	jle    f0101263 <readline+0x82>
			if (echoing)
f010124d:	85 ff                	test   %edi,%edi
f010124f:	74 0d                	je     f010125e <readline+0x7d>
				cputchar('\b');
f0101251:	83 ec 0c             	sub    $0xc,%esp
f0101254:	6a 08                	push   $0x8
f0101256:	e8 d9 f3 ff ff       	call   f0100634 <cputchar>
f010125b:	83 c4 10             	add    $0x10,%esp
			i--;
f010125e:	83 ee 01             	sub    $0x1,%esi
f0101261:	eb b3                	jmp    f0101216 <readline+0x35>
		} else if (c >= ' ' && i < BUFLEN-1) {
f0101263:	83 fb 1f             	cmp    $0x1f,%ebx
f0101266:	7e 23                	jle    f010128b <readline+0xaa>
f0101268:	81 fe fe 03 00 00    	cmp    $0x3fe,%esi
f010126e:	7f 1b                	jg     f010128b <readline+0xaa>
			if (echoing)
f0101270:	85 ff                	test   %edi,%edi
f0101272:	74 0c                	je     f0101280 <readline+0x9f>
				cputchar(c);
f0101274:	83 ec 0c             	sub    $0xc,%esp
f0101277:	53                   	push   %ebx
f0101278:	e8 b7 f3 ff ff       	call   f0100634 <cputchar>
f010127d:	83 c4 10             	add    $0x10,%esp
			buf[i++] = c;
f0101280:	88 9e 40 25 11 f0    	mov    %bl,-0xfeedac0(%esi)
f0101286:	8d 76 01             	lea    0x1(%esi),%esi
f0101289:	eb 8b                	jmp    f0101216 <readline+0x35>
		} else if (c == '\n' || c == '\r') {
f010128b:	83 fb 0a             	cmp    $0xa,%ebx
f010128e:	74 05                	je     f0101295 <readline+0xb4>
f0101290:	83 fb 0d             	cmp    $0xd,%ebx
f0101293:	75 81                	jne    f0101216 <readline+0x35>
			if (echoing)
f0101295:	85 ff                	test   %edi,%edi
f0101297:	74 0d                	je     f01012a6 <readline+0xc5>
				cputchar('\n');
f0101299:	83 ec 0c             	sub    $0xc,%esp
f010129c:	6a 0a                	push   $0xa
f010129e:	e8 91 f3 ff ff       	call   f0100634 <cputchar>
f01012a3:	83 c4 10             	add    $0x10,%esp
			buf[i] = 0;
f01012a6:	c6 86 40 25 11 f0 00 	movb   $0x0,-0xfeedac0(%esi)
			return buf;
f01012ad:	b8 40 25 11 f0       	mov    $0xf0112540,%eax
		}
	}
}
f01012b2:	8d 65 f4             	lea    -0xc(%ebp),%esp
f01012b5:	5b                   	pop    %ebx
f01012b6:	5e                   	pop    %esi
f01012b7:	5f                   	pop    %edi
f01012b8:	5d                   	pop    %ebp
f01012b9:	c3                   	ret    

f01012ba <strlen>:
// Primespipe runs 3x faster this way.
#define ASM 1

int
strlen(const char *s)
{
f01012ba:	55                   	push   %ebp
f01012bb:	89 e5                	mov    %esp,%ebp
f01012bd:	8b 55 08             	mov    0x8(%ebp),%edx
	int n;

	for (n = 0; *s != '\0'; s++)
f01012c0:	b8 00 00 00 00       	mov    $0x0,%eax
f01012c5:	eb 03                	jmp    f01012ca <strlen+0x10>
		n++;
f01012c7:	83 c0 01             	add    $0x1,%eax
int
strlen(const char *s)
{
	int n;

	for (n = 0; *s != '\0'; s++)
f01012ca:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
f01012ce:	75 f7                	jne    f01012c7 <strlen+0xd>
		n++;
	return n;
}
f01012d0:	5d                   	pop    %ebp
f01012d1:	c3                   	ret    

f01012d2 <strnlen>:

int
strnlen(const char *s, size_t size)
{
f01012d2:	55                   	push   %ebp
f01012d3:	89 e5                	mov    %esp,%ebp
f01012d5:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01012d8:	8b 45 0c             	mov    0xc(%ebp),%eax
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f01012db:	ba 00 00 00 00       	mov    $0x0,%edx
f01012e0:	eb 03                	jmp    f01012e5 <strnlen+0x13>
		n++;
f01012e2:	83 c2 01             	add    $0x1,%edx
int
strnlen(const char *s, size_t size)
{
	int n;

	for (n = 0; size > 0 && *s != '\0'; s++, size--)
f01012e5:	39 c2                	cmp    %eax,%edx
f01012e7:	74 08                	je     f01012f1 <strnlen+0x1f>
f01012e9:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
f01012ed:	75 f3                	jne    f01012e2 <strnlen+0x10>
f01012ef:	89 d0                	mov    %edx,%eax
		n++;
	return n;
}
f01012f1:	5d                   	pop    %ebp
f01012f2:	c3                   	ret    

f01012f3 <strcpy>:

char *
strcpy(char *dst, const char *src)
{
f01012f3:	55                   	push   %ebp
f01012f4:	89 e5                	mov    %esp,%ebp
f01012f6:	53                   	push   %ebx
f01012f7:	8b 45 08             	mov    0x8(%ebp),%eax
f01012fa:	8b 4d 0c             	mov    0xc(%ebp),%ecx
	char *ret;

	ret = dst;
	while ((*dst++ = *src++) != '\0')
f01012fd:	89 c2                	mov    %eax,%edx
f01012ff:	83 c2 01             	add    $0x1,%edx
f0101302:	83 c1 01             	add    $0x1,%ecx
f0101305:	0f b6 59 ff          	movzbl -0x1(%ecx),%ebx
f0101309:	88 5a ff             	mov    %bl,-0x1(%edx)
f010130c:	84 db                	test   %bl,%bl
f010130e:	75 ef                	jne    f01012ff <strcpy+0xc>
		/* do nothing */;
	return ret;
}
f0101310:	5b                   	pop    %ebx
f0101311:	5d                   	pop    %ebp
f0101312:	c3                   	ret    

f0101313 <strcat>:

char *
strcat(char *dst, const char *src)
{
f0101313:	55                   	push   %ebp
f0101314:	89 e5                	mov    %esp,%ebp
f0101316:	53                   	push   %ebx
f0101317:	8b 5d 08             	mov    0x8(%ebp),%ebx
	int len = strlen(dst);
f010131a:	53                   	push   %ebx
f010131b:	e8 9a ff ff ff       	call   f01012ba <strlen>
f0101320:	83 c4 04             	add    $0x4,%esp
	strcpy(dst + len, src);
f0101323:	ff 75 0c             	pushl  0xc(%ebp)
f0101326:	01 d8                	add    %ebx,%eax
f0101328:	50                   	push   %eax
f0101329:	e8 c5 ff ff ff       	call   f01012f3 <strcpy>
	return dst;
}
f010132e:	89 d8                	mov    %ebx,%eax
f0101330:	8b 5d fc             	mov    -0x4(%ebp),%ebx
f0101333:	c9                   	leave  
f0101334:	c3                   	ret    

f0101335 <strncpy>:

char *
strncpy(char *dst, const char *src, size_t size) {
f0101335:	55                   	push   %ebp
f0101336:	89 e5                	mov    %esp,%ebp
f0101338:	56                   	push   %esi
f0101339:	53                   	push   %ebx
f010133a:	8b 75 08             	mov    0x8(%ebp),%esi
f010133d:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f0101340:	89 f3                	mov    %esi,%ebx
f0101342:	03 5d 10             	add    0x10(%ebp),%ebx
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0101345:	89 f2                	mov    %esi,%edx
f0101347:	eb 0f                	jmp    f0101358 <strncpy+0x23>
		*dst++ = *src;
f0101349:	83 c2 01             	add    $0x1,%edx
f010134c:	0f b6 01             	movzbl (%ecx),%eax
f010134f:	88 42 ff             	mov    %al,-0x1(%edx)
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
f0101352:	80 39 01             	cmpb   $0x1,(%ecx)
f0101355:	83 d9 ff             	sbb    $0xffffffff,%ecx
strncpy(char *dst, const char *src, size_t size) {
	size_t i;
	char *ret;

	ret = dst;
	for (i = 0; i < size; i++) {
f0101358:	39 da                	cmp    %ebx,%edx
f010135a:	75 ed                	jne    f0101349 <strncpy+0x14>
		// If strlen(src) < size, null-pad 'dst' out to 'size' chars
		if (*src != '\0')
			src++;
	}
	return ret;
}
f010135c:	89 f0                	mov    %esi,%eax
f010135e:	5b                   	pop    %ebx
f010135f:	5e                   	pop    %esi
f0101360:	5d                   	pop    %ebp
f0101361:	c3                   	ret    

f0101362 <strlcpy>:

size_t
strlcpy(char *dst, const char *src, size_t size)
{
f0101362:	55                   	push   %ebp
f0101363:	89 e5                	mov    %esp,%ebp
f0101365:	56                   	push   %esi
f0101366:	53                   	push   %ebx
f0101367:	8b 75 08             	mov    0x8(%ebp),%esi
f010136a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
f010136d:	8b 55 10             	mov    0x10(%ebp),%edx
f0101370:	89 f0                	mov    %esi,%eax
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
f0101372:	85 d2                	test   %edx,%edx
f0101374:	74 21                	je     f0101397 <strlcpy+0x35>
f0101376:	8d 44 16 ff          	lea    -0x1(%esi,%edx,1),%eax
f010137a:	89 f2                	mov    %esi,%edx
f010137c:	eb 09                	jmp    f0101387 <strlcpy+0x25>
		while (--size > 0 && *src != '\0')
			*dst++ = *src++;
f010137e:	83 c2 01             	add    $0x1,%edx
f0101381:	83 c1 01             	add    $0x1,%ecx
f0101384:	88 5a ff             	mov    %bl,-0x1(%edx)
{
	char *dst_in;

	dst_in = dst;
	if (size > 0) {
		while (--size > 0 && *src != '\0')
f0101387:	39 c2                	cmp    %eax,%edx
f0101389:	74 09                	je     f0101394 <strlcpy+0x32>
f010138b:	0f b6 19             	movzbl (%ecx),%ebx
f010138e:	84 db                	test   %bl,%bl
f0101390:	75 ec                	jne    f010137e <strlcpy+0x1c>
f0101392:	89 d0                	mov    %edx,%eax
			*dst++ = *src++;
		*dst = '\0';
f0101394:	c6 00 00             	movb   $0x0,(%eax)
	}
	return dst - dst_in;
f0101397:	29 f0                	sub    %esi,%eax
}
f0101399:	5b                   	pop    %ebx
f010139a:	5e                   	pop    %esi
f010139b:	5d                   	pop    %ebp
f010139c:	c3                   	ret    

f010139d <strcmp>:

int
strcmp(const char *p, const char *q)
{
f010139d:	55                   	push   %ebp
f010139e:	89 e5                	mov    %esp,%ebp
f01013a0:	8b 4d 08             	mov    0x8(%ebp),%ecx
f01013a3:	8b 55 0c             	mov    0xc(%ebp),%edx
	while (*p && *p == *q)
f01013a6:	eb 06                	jmp    f01013ae <strcmp+0x11>
		p++, q++;
f01013a8:	83 c1 01             	add    $0x1,%ecx
f01013ab:	83 c2 01             	add    $0x1,%edx
}

int
strcmp(const char *p, const char *q)
{
	while (*p && *p == *q)
f01013ae:	0f b6 01             	movzbl (%ecx),%eax
f01013b1:	84 c0                	test   %al,%al
f01013b3:	74 04                	je     f01013b9 <strcmp+0x1c>
f01013b5:	3a 02                	cmp    (%edx),%al
f01013b7:	74 ef                	je     f01013a8 <strcmp+0xb>
		p++, q++;
	return (int) ((unsigned char) *p - (unsigned char) *q);
f01013b9:	0f b6 c0             	movzbl %al,%eax
f01013bc:	0f b6 12             	movzbl (%edx),%edx
f01013bf:	29 d0                	sub    %edx,%eax
}
f01013c1:	5d                   	pop    %ebp
f01013c2:	c3                   	ret    

f01013c3 <strncmp>:

int
strncmp(const char *p, const char *q, size_t n)
{
f01013c3:	55                   	push   %ebp
f01013c4:	89 e5                	mov    %esp,%ebp
f01013c6:	53                   	push   %ebx
f01013c7:	8b 45 08             	mov    0x8(%ebp),%eax
f01013ca:	8b 55 0c             	mov    0xc(%ebp),%edx
f01013cd:	89 c3                	mov    %eax,%ebx
f01013cf:	03 5d 10             	add    0x10(%ebp),%ebx
	while (n > 0 && *p && *p == *q)
f01013d2:	eb 06                	jmp    f01013da <strncmp+0x17>
		n--, p++, q++;
f01013d4:	83 c0 01             	add    $0x1,%eax
f01013d7:	83 c2 01             	add    $0x1,%edx
}

int
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
f01013da:	39 d8                	cmp    %ebx,%eax
f01013dc:	74 15                	je     f01013f3 <strncmp+0x30>
f01013de:	0f b6 08             	movzbl (%eax),%ecx
f01013e1:	84 c9                	test   %cl,%cl
f01013e3:	74 04                	je     f01013e9 <strncmp+0x26>
f01013e5:	3a 0a                	cmp    (%edx),%cl
f01013e7:	74 eb                	je     f01013d4 <strncmp+0x11>
		n--, p++, q++;
	if (n == 0)
		return 0;
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
f01013e9:	0f b6 00             	movzbl (%eax),%eax
f01013ec:	0f b6 12             	movzbl (%edx),%edx
f01013ef:	29 d0                	sub    %edx,%eax
f01013f1:	eb 05                	jmp    f01013f8 <strncmp+0x35>
strncmp(const char *p, const char *q, size_t n)
{
	while (n > 0 && *p && *p == *q)
		n--, p++, q++;
	if (n == 0)
		return 0;
f01013f3:	b8 00 00 00 00       	mov    $0x0,%eax
	else
		return (int) ((unsigned char) *p - (unsigned char) *q);
}
f01013f8:	5b                   	pop    %ebx
f01013f9:	5d                   	pop    %ebp
f01013fa:	c3                   	ret    

f01013fb <strchr>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
f01013fb:	55                   	push   %ebp
f01013fc:	89 e5                	mov    %esp,%ebp
f01013fe:	8b 45 08             	mov    0x8(%ebp),%eax
f0101401:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
	for (; *s; s++)
f0101405:	eb 07                	jmp    f010140e <strchr+0x13>
		if (*s == c)
f0101407:	38 ca                	cmp    %cl,%dl
f0101409:	74 0f                	je     f010141a <strchr+0x1f>
// Return a pointer to the first occurrence of 'c' in 's',
// or a null pointer if the string has no 'c'.
char *
strchr(const char *s, char c)
{
	for (; *s; s++)
f010140b:	83 c0 01             	add    $0x1,%eax
f010140e:	0f b6 10             	movzbl (%eax),%edx
f0101411:	84 d2                	test   %dl,%dl
f0101413:	75 f2                	jne    f0101407 <strchr+0xc>
		if (*s == c)
			return (char *) s;
	return 0;
f0101415:	b8 00 00 00 00       	mov    $0x0,%eax
}
f010141a:	5d                   	pop    %ebp
f010141b:	c3                   	ret    

f010141c <strfind>:

// Return a pointer to the first occurrence of 'c' in 's',
// or a pointer to the string-ending null character if the string has no 'c'.
char *
strfind(const char *s, char c)
{
f010141c:	55                   	push   %ebp
f010141d:	89 e5                	mov    %esp,%ebp
f010141f:	8b 45 08             	mov    0x8(%ebp),%eax
f0101422:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
	for (; *s; s++)
f0101426:	eb 03                	jmp    f010142b <strfind+0xf>
f0101428:	83 c0 01             	add    $0x1,%eax
f010142b:	0f b6 10             	movzbl (%eax),%edx
		if (*s == c)
f010142e:	38 ca                	cmp    %cl,%dl
f0101430:	74 04                	je     f0101436 <strfind+0x1a>
f0101432:	84 d2                	test   %dl,%dl
f0101434:	75 f2                	jne    f0101428 <strfind+0xc>
			break;
	return (char *) s;
}
f0101436:	5d                   	pop    %ebp
f0101437:	c3                   	ret    

f0101438 <memset>:

#if ASM
void *
memset(void *v, int c, size_t n)
{
f0101438:	55                   	push   %ebp
f0101439:	89 e5                	mov    %esp,%ebp
f010143b:	57                   	push   %edi
f010143c:	56                   	push   %esi
f010143d:	53                   	push   %ebx
f010143e:	8b 7d 08             	mov    0x8(%ebp),%edi
f0101441:	8b 4d 10             	mov    0x10(%ebp),%ecx
	char *p;

	if (n == 0)
f0101444:	85 c9                	test   %ecx,%ecx
f0101446:	74 36                	je     f010147e <memset+0x46>
		return v;
	if ((int)v%4 == 0 && n%4 == 0) {
f0101448:	f7 c7 03 00 00 00    	test   $0x3,%edi
f010144e:	75 28                	jne    f0101478 <memset+0x40>
f0101450:	f6 c1 03             	test   $0x3,%cl
f0101453:	75 23                	jne    f0101478 <memset+0x40>
		c &= 0xFF;
f0101455:	0f b6 55 0c          	movzbl 0xc(%ebp),%edx
		c = (c<<24)|(c<<16)|(c<<8)|c;
f0101459:	89 d3                	mov    %edx,%ebx
f010145b:	c1 e3 08             	shl    $0x8,%ebx
f010145e:	89 d6                	mov    %edx,%esi
f0101460:	c1 e6 18             	shl    $0x18,%esi
f0101463:	89 d0                	mov    %edx,%eax
f0101465:	c1 e0 10             	shl    $0x10,%eax
f0101468:	09 f0                	or     %esi,%eax
f010146a:	09 c2                	or     %eax,%edx
		asm volatile("cld; rep stosl\n"
f010146c:	89 d8                	mov    %ebx,%eax
f010146e:	09 d0                	or     %edx,%eax
f0101470:	c1 e9 02             	shr    $0x2,%ecx
f0101473:	fc                   	cld    
f0101474:	f3 ab                	rep stos %eax,%es:(%edi)
f0101476:	eb 06                	jmp    f010147e <memset+0x46>
			:: "D" (v), "a" (c), "c" (n/4)
			: "cc", "memory");
	} else
		asm volatile("cld; rep stosb\n"
f0101478:	8b 45 0c             	mov    0xc(%ebp),%eax
f010147b:	fc                   	cld    
f010147c:	f3 aa                	rep stos %al,%es:(%edi)
			:: "D" (v), "a" (c), "c" (n)
			: "cc", "memory");
	return v;
}
f010147e:	89 f8                	mov    %edi,%eax
f0101480:	5b                   	pop    %ebx
f0101481:	5e                   	pop    %esi
f0101482:	5f                   	pop    %edi
f0101483:	5d                   	pop    %ebp
f0101484:	c3                   	ret    

f0101485 <memmove>:

void *
memmove(void *dst, const void *src, size_t n)
{
f0101485:	55                   	push   %ebp
f0101486:	89 e5                	mov    %esp,%ebp
f0101488:	57                   	push   %edi
f0101489:	56                   	push   %esi
f010148a:	8b 45 08             	mov    0x8(%ebp),%eax
f010148d:	8b 75 0c             	mov    0xc(%ebp),%esi
f0101490:	8b 4d 10             	mov    0x10(%ebp),%ecx
	const char *s;
	char *d;

	s = src;
	d = dst;
	if (s < d && s + n > d) {
f0101493:	39 c6                	cmp    %eax,%esi
f0101495:	73 35                	jae    f01014cc <memmove+0x47>
f0101497:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
f010149a:	39 d0                	cmp    %edx,%eax
f010149c:	73 2e                	jae    f01014cc <memmove+0x47>
		s += n;
		d += n;
f010149e:	8d 3c 08             	lea    (%eax,%ecx,1),%edi
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f01014a1:	89 d6                	mov    %edx,%esi
f01014a3:	09 fe                	or     %edi,%esi
f01014a5:	f7 c6 03 00 00 00    	test   $0x3,%esi
f01014ab:	75 13                	jne    f01014c0 <memmove+0x3b>
f01014ad:	f6 c1 03             	test   $0x3,%cl
f01014b0:	75 0e                	jne    f01014c0 <memmove+0x3b>
			asm volatile("std; rep movsl\n"
f01014b2:	83 ef 04             	sub    $0x4,%edi
f01014b5:	8d 72 fc             	lea    -0x4(%edx),%esi
f01014b8:	c1 e9 02             	shr    $0x2,%ecx
f01014bb:	fd                   	std    
f01014bc:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01014be:	eb 09                	jmp    f01014c9 <memmove+0x44>
				:: "D" (d-4), "S" (s-4), "c" (n/4) : "cc", "memory");
		else
			asm volatile("std; rep movsb\n"
f01014c0:	83 ef 01             	sub    $0x1,%edi
f01014c3:	8d 72 ff             	lea    -0x1(%edx),%esi
f01014c6:	fd                   	std    
f01014c7:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d-1), "S" (s-1), "c" (n) : "cc", "memory");
		// Some versions of GCC rely on DF being clear
		asm volatile("cld" ::: "cc");
f01014c9:	fc                   	cld    
f01014ca:	eb 1d                	jmp    f01014e9 <memmove+0x64>
	} else {
		if ((int)s%4 == 0 && (int)d%4 == 0 && n%4 == 0)
f01014cc:	89 f2                	mov    %esi,%edx
f01014ce:	09 c2                	or     %eax,%edx
f01014d0:	f6 c2 03             	test   $0x3,%dl
f01014d3:	75 0f                	jne    f01014e4 <memmove+0x5f>
f01014d5:	f6 c1 03             	test   $0x3,%cl
f01014d8:	75 0a                	jne    f01014e4 <memmove+0x5f>
			asm volatile("cld; rep movsl\n"
f01014da:	c1 e9 02             	shr    $0x2,%ecx
f01014dd:	89 c7                	mov    %eax,%edi
f01014df:	fc                   	cld    
f01014e0:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
f01014e2:	eb 05                	jmp    f01014e9 <memmove+0x64>
				:: "D" (d), "S" (s), "c" (n/4) : "cc", "memory");
		else
			asm volatile("cld; rep movsb\n"
f01014e4:	89 c7                	mov    %eax,%edi
f01014e6:	fc                   	cld    
f01014e7:	f3 a4                	rep movsb %ds:(%esi),%es:(%edi)
				:: "D" (d), "S" (s), "c" (n) : "cc", "memory");
	}
	return dst;
}
f01014e9:	5e                   	pop    %esi
f01014ea:	5f                   	pop    %edi
f01014eb:	5d                   	pop    %ebp
f01014ec:	c3                   	ret    

f01014ed <memcpy>:
}
#endif

void *
memcpy(void *dst, const void *src, size_t n)
{
f01014ed:	55                   	push   %ebp
f01014ee:	89 e5                	mov    %esp,%ebp
	return memmove(dst, src, n);
f01014f0:	ff 75 10             	pushl  0x10(%ebp)
f01014f3:	ff 75 0c             	pushl  0xc(%ebp)
f01014f6:	ff 75 08             	pushl  0x8(%ebp)
f01014f9:	e8 87 ff ff ff       	call   f0101485 <memmove>
}
f01014fe:	c9                   	leave  
f01014ff:	c3                   	ret    

f0101500 <memcmp>:

int
memcmp(const void *v1, const void *v2, size_t n)
{
f0101500:	55                   	push   %ebp
f0101501:	89 e5                	mov    %esp,%ebp
f0101503:	56                   	push   %esi
f0101504:	53                   	push   %ebx
f0101505:	8b 45 08             	mov    0x8(%ebp),%eax
f0101508:	8b 55 0c             	mov    0xc(%ebp),%edx
f010150b:	89 c6                	mov    %eax,%esi
f010150d:	03 75 10             	add    0x10(%ebp),%esi
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f0101510:	eb 1a                	jmp    f010152c <memcmp+0x2c>
		if (*s1 != *s2)
f0101512:	0f b6 08             	movzbl (%eax),%ecx
f0101515:	0f b6 1a             	movzbl (%edx),%ebx
f0101518:	38 d9                	cmp    %bl,%cl
f010151a:	74 0a                	je     f0101526 <memcmp+0x26>
			return (int) *s1 - (int) *s2;
f010151c:	0f b6 c1             	movzbl %cl,%eax
f010151f:	0f b6 db             	movzbl %bl,%ebx
f0101522:	29 d8                	sub    %ebx,%eax
f0101524:	eb 0f                	jmp    f0101535 <memcmp+0x35>
		s1++, s2++;
f0101526:	83 c0 01             	add    $0x1,%eax
f0101529:	83 c2 01             	add    $0x1,%edx
memcmp(const void *v1, const void *v2, size_t n)
{
	const uint8_t *s1 = (const uint8_t *) v1;
	const uint8_t *s2 = (const uint8_t *) v2;

	while (n-- > 0) {
f010152c:	39 f0                	cmp    %esi,%eax
f010152e:	75 e2                	jne    f0101512 <memcmp+0x12>
		if (*s1 != *s2)
			return (int) *s1 - (int) *s2;
		s1++, s2++;
	}

	return 0;
f0101530:	b8 00 00 00 00       	mov    $0x0,%eax
}
f0101535:	5b                   	pop    %ebx
f0101536:	5e                   	pop    %esi
f0101537:	5d                   	pop    %ebp
f0101538:	c3                   	ret    

f0101539 <memfind>:

void *
memfind(const void *s, int c, size_t n)
{
f0101539:	55                   	push   %ebp
f010153a:	89 e5                	mov    %esp,%ebp
f010153c:	53                   	push   %ebx
f010153d:	8b 45 08             	mov    0x8(%ebp),%eax
	const void *ends = (const char *) s + n;
f0101540:	89 c1                	mov    %eax,%ecx
f0101542:	03 4d 10             	add    0x10(%ebp),%ecx
	for (; s < ends; s++)
		if (*(const unsigned char *) s == (unsigned char) c)
f0101545:	0f b6 5d 0c          	movzbl 0xc(%ebp),%ebx

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0101549:	eb 0a                	jmp    f0101555 <memfind+0x1c>
		if (*(const unsigned char *) s == (unsigned char) c)
f010154b:	0f b6 10             	movzbl (%eax),%edx
f010154e:	39 da                	cmp    %ebx,%edx
f0101550:	74 07                	je     f0101559 <memfind+0x20>

void *
memfind(const void *s, int c, size_t n)
{
	const void *ends = (const char *) s + n;
	for (; s < ends; s++)
f0101552:	83 c0 01             	add    $0x1,%eax
f0101555:	39 c8                	cmp    %ecx,%eax
f0101557:	72 f2                	jb     f010154b <memfind+0x12>
		if (*(const unsigned char *) s == (unsigned char) c)
			break;
	return (void *) s;
}
f0101559:	5b                   	pop    %ebx
f010155a:	5d                   	pop    %ebp
f010155b:	c3                   	ret    

f010155c <strtol>:

long
strtol(const char *s, char **endptr, int base)
{
f010155c:	55                   	push   %ebp
f010155d:	89 e5                	mov    %esp,%ebp
f010155f:	57                   	push   %edi
f0101560:	56                   	push   %esi
f0101561:	53                   	push   %ebx
f0101562:	8b 55 08             	mov    0x8(%ebp),%edx
f0101565:	8b 5d 10             	mov    0x10(%ebp),%ebx
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f0101568:	eb 03                	jmp    f010156d <strtol+0x11>
		s++;
f010156a:	83 c2 01             	add    $0x1,%edx
{
	int neg = 0;
	long val = 0;

	// gobble initial whitespace
	while (*s == ' ' || *s == '\t')
f010156d:	0f b6 02             	movzbl (%edx),%eax
f0101570:	3c 20                	cmp    $0x20,%al
f0101572:	74 f6                	je     f010156a <strtol+0xe>
f0101574:	3c 09                	cmp    $0x9,%al
f0101576:	74 f2                	je     f010156a <strtol+0xe>
		s++;

	// plus/minus sign
	if (*s == '+')
f0101578:	3c 2b                	cmp    $0x2b,%al
f010157a:	75 0a                	jne    f0101586 <strtol+0x2a>
		s++;
f010157c:	83 c2 01             	add    $0x1,%edx
}

long
strtol(const char *s, char **endptr, int base)
{
	int neg = 0;
f010157f:	bf 00 00 00 00       	mov    $0x0,%edi
f0101584:	eb 11                	jmp    f0101597 <strtol+0x3b>
f0101586:	bf 00 00 00 00       	mov    $0x0,%edi
		s++;

	// plus/minus sign
	if (*s == '+')
		s++;
	else if (*s == '-')
f010158b:	3c 2d                	cmp    $0x2d,%al
f010158d:	75 08                	jne    f0101597 <strtol+0x3b>
		s++, neg = 1;
f010158f:	83 c2 01             	add    $0x1,%edx
f0101592:	bf 01 00 00 00       	mov    $0x1,%edi

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
f0101597:	f7 c3 ef ff ff ff    	test   $0xffffffef,%ebx
f010159d:	75 19                	jne    f01015b8 <strtol+0x5c>
f010159f:	80 3a 30             	cmpb   $0x30,(%edx)
f01015a2:	75 14                	jne    f01015b8 <strtol+0x5c>
f01015a4:	80 7a 01 78          	cmpb   $0x78,0x1(%edx)
f01015a8:	0f 85 84 00 00 00    	jne    f0101632 <strtol+0xd6>
		s += 2, base = 16;
f01015ae:	83 c2 02             	add    $0x2,%edx
f01015b1:	bb 10 00 00 00       	mov    $0x10,%ebx
f01015b6:	eb 16                	jmp    f01015ce <strtol+0x72>
	else if (base == 0 && s[0] == '0')
f01015b8:	85 db                	test   %ebx,%ebx
f01015ba:	75 12                	jne    f01015ce <strtol+0x72>
		s++, base = 8;
	else if (base == 0)
		base = 10;
f01015bc:	bb 0a 00 00 00       	mov    $0xa,%ebx
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
		s += 2, base = 16;
	else if (base == 0 && s[0] == '0')
f01015c1:	80 3a 30             	cmpb   $0x30,(%edx)
f01015c4:	75 08                	jne    f01015ce <strtol+0x72>
		s++, base = 8;
f01015c6:	83 c2 01             	add    $0x1,%edx
f01015c9:	bb 08 00 00 00       	mov    $0x8,%ebx
	else if (base == 0)
		base = 10;
f01015ce:	b9 00 00 00 00       	mov    $0x0,%ecx
f01015d3:	89 5d 10             	mov    %ebx,0x10(%ebp)

	// digits
	while (1) {
		int dig;

		if (*s >= '0' && *s <= '9')
f01015d6:	0f b6 02             	movzbl (%edx),%eax
f01015d9:	8d 70 d0             	lea    -0x30(%eax),%esi
f01015dc:	89 f3                	mov    %esi,%ebx
f01015de:	80 fb 09             	cmp    $0x9,%bl
f01015e1:	77 08                	ja     f01015eb <strtol+0x8f>
			dig = *s - '0';
f01015e3:	0f be c0             	movsbl %al,%eax
f01015e6:	83 e8 30             	sub    $0x30,%eax
f01015e9:	eb 22                	jmp    f010160d <strtol+0xb1>
		else if (*s >= 'a' && *s <= 'z')
f01015eb:	8d 70 9f             	lea    -0x61(%eax),%esi
f01015ee:	89 f3                	mov    %esi,%ebx
f01015f0:	80 fb 19             	cmp    $0x19,%bl
f01015f3:	77 08                	ja     f01015fd <strtol+0xa1>
			dig = *s - 'a' + 10;
f01015f5:	0f be c0             	movsbl %al,%eax
f01015f8:	83 e8 57             	sub    $0x57,%eax
f01015fb:	eb 10                	jmp    f010160d <strtol+0xb1>
		else if (*s >= 'A' && *s <= 'Z')
f01015fd:	8d 70 bf             	lea    -0x41(%eax),%esi
f0101600:	89 f3                	mov    %esi,%ebx
f0101602:	80 fb 19             	cmp    $0x19,%bl
f0101605:	77 16                	ja     f010161d <strtol+0xc1>
			dig = *s - 'A' + 10;
f0101607:	0f be c0             	movsbl %al,%eax
f010160a:	83 e8 37             	sub    $0x37,%eax
		else
			break;
		if (dig >= base)
f010160d:	3b 45 10             	cmp    0x10(%ebp),%eax
f0101610:	7d 0b                	jge    f010161d <strtol+0xc1>
			break;
		s++, val = (val * base) + dig;
f0101612:	83 c2 01             	add    $0x1,%edx
f0101615:	0f af 4d 10          	imul   0x10(%ebp),%ecx
f0101619:	01 c1                	add    %eax,%ecx
		// we don't properly detect overflow!
	}
f010161b:	eb b9                	jmp    f01015d6 <strtol+0x7a>

	if (endptr)
f010161d:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
f0101621:	74 05                	je     f0101628 <strtol+0xcc>
		*endptr = (char *) s;
f0101623:	8b 45 0c             	mov    0xc(%ebp),%eax
f0101626:	89 10                	mov    %edx,(%eax)
f0101628:	89 c8                	mov    %ecx,%eax
	return (neg ? -val : val);
f010162a:	85 ff                	test   %edi,%edi
f010162c:	74 0a                	je     f0101638 <strtol+0xdc>
f010162e:	f7 d8                	neg    %eax
f0101630:	eb 06                	jmp    f0101638 <strtol+0xdc>
		s++, neg = 1;

	// hex or octal base prefix
	if ((base == 0 || base == 16) && (s[0] == '0' && s[1] == 'x'))
		s += 2, base = 16;
	else if (base == 0 && s[0] == '0')
f0101632:	85 db                	test   %ebx,%ebx
f0101634:	74 90                	je     f01015c6 <strtol+0x6a>
f0101636:	eb 96                	jmp    f01015ce <strtol+0x72>
	}

	if (endptr)
		*endptr = (char *) s;
	return (neg ? -val : val);
}
f0101638:	5b                   	pop    %ebx
f0101639:	5e                   	pop    %esi
f010163a:	5f                   	pop    %edi
f010163b:	5d                   	pop    %ebp
f010163c:	c3                   	ret    
f010163d:	66 90                	xchg   %ax,%ax
f010163f:	90                   	nop

f0101640 <__udivdi3>:
f0101640:	55                   	push   %ebp
f0101641:	57                   	push   %edi
f0101642:	56                   	push   %esi
f0101643:	53                   	push   %ebx
f0101644:	83 ec 1c             	sub    $0x1c,%esp
f0101647:	8b 74 24 3c          	mov    0x3c(%esp),%esi
f010164b:	8b 5c 24 30          	mov    0x30(%esp),%ebx
f010164f:	8b 4c 24 34          	mov    0x34(%esp),%ecx
f0101653:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0101657:	85 f6                	test   %esi,%esi
f0101659:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f010165d:	89 ca                	mov    %ecx,%edx
f010165f:	89 f8                	mov    %edi,%eax
f0101661:	75 3d                	jne    f01016a0 <__udivdi3+0x60>
f0101663:	39 cf                	cmp    %ecx,%edi
f0101665:	0f 87 c5 00 00 00    	ja     f0101730 <__udivdi3+0xf0>
f010166b:	85 ff                	test   %edi,%edi
f010166d:	89 fd                	mov    %edi,%ebp
f010166f:	75 0b                	jne    f010167c <__udivdi3+0x3c>
f0101671:	b8 01 00 00 00       	mov    $0x1,%eax
f0101676:	31 d2                	xor    %edx,%edx
f0101678:	f7 f7                	div    %edi
f010167a:	89 c5                	mov    %eax,%ebp
f010167c:	89 c8                	mov    %ecx,%eax
f010167e:	31 d2                	xor    %edx,%edx
f0101680:	f7 f5                	div    %ebp
f0101682:	89 c1                	mov    %eax,%ecx
f0101684:	89 d8                	mov    %ebx,%eax
f0101686:	89 cf                	mov    %ecx,%edi
f0101688:	f7 f5                	div    %ebp
f010168a:	89 c3                	mov    %eax,%ebx
f010168c:	89 d8                	mov    %ebx,%eax
f010168e:	89 fa                	mov    %edi,%edx
f0101690:	83 c4 1c             	add    $0x1c,%esp
f0101693:	5b                   	pop    %ebx
f0101694:	5e                   	pop    %esi
f0101695:	5f                   	pop    %edi
f0101696:	5d                   	pop    %ebp
f0101697:	c3                   	ret    
f0101698:	90                   	nop
f0101699:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f01016a0:	39 ce                	cmp    %ecx,%esi
f01016a2:	77 74                	ja     f0101718 <__udivdi3+0xd8>
f01016a4:	0f bd fe             	bsr    %esi,%edi
f01016a7:	83 f7 1f             	xor    $0x1f,%edi
f01016aa:	0f 84 98 00 00 00    	je     f0101748 <__udivdi3+0x108>
f01016b0:	bb 20 00 00 00       	mov    $0x20,%ebx
f01016b5:	89 f9                	mov    %edi,%ecx
f01016b7:	89 c5                	mov    %eax,%ebp
f01016b9:	29 fb                	sub    %edi,%ebx
f01016bb:	d3 e6                	shl    %cl,%esi
f01016bd:	89 d9                	mov    %ebx,%ecx
f01016bf:	d3 ed                	shr    %cl,%ebp
f01016c1:	89 f9                	mov    %edi,%ecx
f01016c3:	d3 e0                	shl    %cl,%eax
f01016c5:	09 ee                	or     %ebp,%esi
f01016c7:	89 d9                	mov    %ebx,%ecx
f01016c9:	89 44 24 0c          	mov    %eax,0xc(%esp)
f01016cd:	89 d5                	mov    %edx,%ebp
f01016cf:	8b 44 24 08          	mov    0x8(%esp),%eax
f01016d3:	d3 ed                	shr    %cl,%ebp
f01016d5:	89 f9                	mov    %edi,%ecx
f01016d7:	d3 e2                	shl    %cl,%edx
f01016d9:	89 d9                	mov    %ebx,%ecx
f01016db:	d3 e8                	shr    %cl,%eax
f01016dd:	09 c2                	or     %eax,%edx
f01016df:	89 d0                	mov    %edx,%eax
f01016e1:	89 ea                	mov    %ebp,%edx
f01016e3:	f7 f6                	div    %esi
f01016e5:	89 d5                	mov    %edx,%ebp
f01016e7:	89 c3                	mov    %eax,%ebx
f01016e9:	f7 64 24 0c          	mull   0xc(%esp)
f01016ed:	39 d5                	cmp    %edx,%ebp
f01016ef:	72 10                	jb     f0101701 <__udivdi3+0xc1>
f01016f1:	8b 74 24 08          	mov    0x8(%esp),%esi
f01016f5:	89 f9                	mov    %edi,%ecx
f01016f7:	d3 e6                	shl    %cl,%esi
f01016f9:	39 c6                	cmp    %eax,%esi
f01016fb:	73 07                	jae    f0101704 <__udivdi3+0xc4>
f01016fd:	39 d5                	cmp    %edx,%ebp
f01016ff:	75 03                	jne    f0101704 <__udivdi3+0xc4>
f0101701:	83 eb 01             	sub    $0x1,%ebx
f0101704:	31 ff                	xor    %edi,%edi
f0101706:	89 d8                	mov    %ebx,%eax
f0101708:	89 fa                	mov    %edi,%edx
f010170a:	83 c4 1c             	add    $0x1c,%esp
f010170d:	5b                   	pop    %ebx
f010170e:	5e                   	pop    %esi
f010170f:	5f                   	pop    %edi
f0101710:	5d                   	pop    %ebp
f0101711:	c3                   	ret    
f0101712:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f0101718:	31 ff                	xor    %edi,%edi
f010171a:	31 db                	xor    %ebx,%ebx
f010171c:	89 d8                	mov    %ebx,%eax
f010171e:	89 fa                	mov    %edi,%edx
f0101720:	83 c4 1c             	add    $0x1c,%esp
f0101723:	5b                   	pop    %ebx
f0101724:	5e                   	pop    %esi
f0101725:	5f                   	pop    %edi
f0101726:	5d                   	pop    %ebp
f0101727:	c3                   	ret    
f0101728:	90                   	nop
f0101729:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f0101730:	89 d8                	mov    %ebx,%eax
f0101732:	f7 f7                	div    %edi
f0101734:	31 ff                	xor    %edi,%edi
f0101736:	89 c3                	mov    %eax,%ebx
f0101738:	89 d8                	mov    %ebx,%eax
f010173a:	89 fa                	mov    %edi,%edx
f010173c:	83 c4 1c             	add    $0x1c,%esp
f010173f:	5b                   	pop    %ebx
f0101740:	5e                   	pop    %esi
f0101741:	5f                   	pop    %edi
f0101742:	5d                   	pop    %ebp
f0101743:	c3                   	ret    
f0101744:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0101748:	39 ce                	cmp    %ecx,%esi
f010174a:	72 0c                	jb     f0101758 <__udivdi3+0x118>
f010174c:	31 db                	xor    %ebx,%ebx
f010174e:	3b 44 24 08          	cmp    0x8(%esp),%eax
f0101752:	0f 87 34 ff ff ff    	ja     f010168c <__udivdi3+0x4c>
f0101758:	bb 01 00 00 00       	mov    $0x1,%ebx
f010175d:	e9 2a ff ff ff       	jmp    f010168c <__udivdi3+0x4c>
f0101762:	66 90                	xchg   %ax,%ax
f0101764:	66 90                	xchg   %ax,%ax
f0101766:	66 90                	xchg   %ax,%ax
f0101768:	66 90                	xchg   %ax,%ax
f010176a:	66 90                	xchg   %ax,%ax
f010176c:	66 90                	xchg   %ax,%ax
f010176e:	66 90                	xchg   %ax,%ax

f0101770 <__umoddi3>:
f0101770:	55                   	push   %ebp
f0101771:	57                   	push   %edi
f0101772:	56                   	push   %esi
f0101773:	53                   	push   %ebx
f0101774:	83 ec 1c             	sub    $0x1c,%esp
f0101777:	8b 54 24 3c          	mov    0x3c(%esp),%edx
f010177b:	8b 4c 24 30          	mov    0x30(%esp),%ecx
f010177f:	8b 74 24 34          	mov    0x34(%esp),%esi
f0101783:	8b 7c 24 38          	mov    0x38(%esp),%edi
f0101787:	85 d2                	test   %edx,%edx
f0101789:	89 4c 24 0c          	mov    %ecx,0xc(%esp)
f010178d:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f0101791:	89 f3                	mov    %esi,%ebx
f0101793:	89 3c 24             	mov    %edi,(%esp)
f0101796:	89 74 24 04          	mov    %esi,0x4(%esp)
f010179a:	75 1c                	jne    f01017b8 <__umoddi3+0x48>
f010179c:	39 f7                	cmp    %esi,%edi
f010179e:	76 50                	jbe    f01017f0 <__umoddi3+0x80>
f01017a0:	89 c8                	mov    %ecx,%eax
f01017a2:	89 f2                	mov    %esi,%edx
f01017a4:	f7 f7                	div    %edi
f01017a6:	89 d0                	mov    %edx,%eax
f01017a8:	31 d2                	xor    %edx,%edx
f01017aa:	83 c4 1c             	add    $0x1c,%esp
f01017ad:	5b                   	pop    %ebx
f01017ae:	5e                   	pop    %esi
f01017af:	5f                   	pop    %edi
f01017b0:	5d                   	pop    %ebp
f01017b1:	c3                   	ret    
f01017b2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
f01017b8:	39 f2                	cmp    %esi,%edx
f01017ba:	89 d0                	mov    %edx,%eax
f01017bc:	77 52                	ja     f0101810 <__umoddi3+0xa0>
f01017be:	0f bd ea             	bsr    %edx,%ebp
f01017c1:	83 f5 1f             	xor    $0x1f,%ebp
f01017c4:	75 5a                	jne    f0101820 <__umoddi3+0xb0>
f01017c6:	3b 54 24 04          	cmp    0x4(%esp),%edx
f01017ca:	0f 82 e0 00 00 00    	jb     f01018b0 <__umoddi3+0x140>
f01017d0:	39 0c 24             	cmp    %ecx,(%esp)
f01017d3:	0f 86 d7 00 00 00    	jbe    f01018b0 <__umoddi3+0x140>
f01017d9:	8b 44 24 08          	mov    0x8(%esp),%eax
f01017dd:	8b 54 24 04          	mov    0x4(%esp),%edx
f01017e1:	83 c4 1c             	add    $0x1c,%esp
f01017e4:	5b                   	pop    %ebx
f01017e5:	5e                   	pop    %esi
f01017e6:	5f                   	pop    %edi
f01017e7:	5d                   	pop    %ebp
f01017e8:	c3                   	ret    
f01017e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
f01017f0:	85 ff                	test   %edi,%edi
f01017f2:	89 fd                	mov    %edi,%ebp
f01017f4:	75 0b                	jne    f0101801 <__umoddi3+0x91>
f01017f6:	b8 01 00 00 00       	mov    $0x1,%eax
f01017fb:	31 d2                	xor    %edx,%edx
f01017fd:	f7 f7                	div    %edi
f01017ff:	89 c5                	mov    %eax,%ebp
f0101801:	89 f0                	mov    %esi,%eax
f0101803:	31 d2                	xor    %edx,%edx
f0101805:	f7 f5                	div    %ebp
f0101807:	89 c8                	mov    %ecx,%eax
f0101809:	f7 f5                	div    %ebp
f010180b:	89 d0                	mov    %edx,%eax
f010180d:	eb 99                	jmp    f01017a8 <__umoddi3+0x38>
f010180f:	90                   	nop
f0101810:	89 c8                	mov    %ecx,%eax
f0101812:	89 f2                	mov    %esi,%edx
f0101814:	83 c4 1c             	add    $0x1c,%esp
f0101817:	5b                   	pop    %ebx
f0101818:	5e                   	pop    %esi
f0101819:	5f                   	pop    %edi
f010181a:	5d                   	pop    %ebp
f010181b:	c3                   	ret    
f010181c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
f0101820:	8b 34 24             	mov    (%esp),%esi
f0101823:	bf 20 00 00 00       	mov    $0x20,%edi
f0101828:	89 e9                	mov    %ebp,%ecx
f010182a:	29 ef                	sub    %ebp,%edi
f010182c:	d3 e0                	shl    %cl,%eax
f010182e:	89 f9                	mov    %edi,%ecx
f0101830:	89 f2                	mov    %esi,%edx
f0101832:	d3 ea                	shr    %cl,%edx
f0101834:	89 e9                	mov    %ebp,%ecx
f0101836:	09 c2                	or     %eax,%edx
f0101838:	89 d8                	mov    %ebx,%eax
f010183a:	89 14 24             	mov    %edx,(%esp)
f010183d:	89 f2                	mov    %esi,%edx
f010183f:	d3 e2                	shl    %cl,%edx
f0101841:	89 f9                	mov    %edi,%ecx
f0101843:	89 54 24 04          	mov    %edx,0x4(%esp)
f0101847:	8b 54 24 0c          	mov    0xc(%esp),%edx
f010184b:	d3 e8                	shr    %cl,%eax
f010184d:	89 e9                	mov    %ebp,%ecx
f010184f:	89 c6                	mov    %eax,%esi
f0101851:	d3 e3                	shl    %cl,%ebx
f0101853:	89 f9                	mov    %edi,%ecx
f0101855:	89 d0                	mov    %edx,%eax
f0101857:	d3 e8                	shr    %cl,%eax
f0101859:	89 e9                	mov    %ebp,%ecx
f010185b:	09 d8                	or     %ebx,%eax
f010185d:	89 d3                	mov    %edx,%ebx
f010185f:	89 f2                	mov    %esi,%edx
f0101861:	f7 34 24             	divl   (%esp)
f0101864:	89 d6                	mov    %edx,%esi
f0101866:	d3 e3                	shl    %cl,%ebx
f0101868:	f7 64 24 04          	mull   0x4(%esp)
f010186c:	39 d6                	cmp    %edx,%esi
f010186e:	89 5c 24 08          	mov    %ebx,0x8(%esp)
f0101872:	89 d1                	mov    %edx,%ecx
f0101874:	89 c3                	mov    %eax,%ebx
f0101876:	72 08                	jb     f0101880 <__umoddi3+0x110>
f0101878:	75 11                	jne    f010188b <__umoddi3+0x11b>
f010187a:	39 44 24 08          	cmp    %eax,0x8(%esp)
f010187e:	73 0b                	jae    f010188b <__umoddi3+0x11b>
f0101880:	2b 44 24 04          	sub    0x4(%esp),%eax
f0101884:	1b 14 24             	sbb    (%esp),%edx
f0101887:	89 d1                	mov    %edx,%ecx
f0101889:	89 c3                	mov    %eax,%ebx
f010188b:	8b 54 24 08          	mov    0x8(%esp),%edx
f010188f:	29 da                	sub    %ebx,%edx
f0101891:	19 ce                	sbb    %ecx,%esi
f0101893:	89 f9                	mov    %edi,%ecx
f0101895:	89 f0                	mov    %esi,%eax
f0101897:	d3 e0                	shl    %cl,%eax
f0101899:	89 e9                	mov    %ebp,%ecx
f010189b:	d3 ea                	shr    %cl,%edx
f010189d:	89 e9                	mov    %ebp,%ecx
f010189f:	d3 ee                	shr    %cl,%esi
f01018a1:	09 d0                	or     %edx,%eax
f01018a3:	89 f2                	mov    %esi,%edx
f01018a5:	83 c4 1c             	add    $0x1c,%esp
f01018a8:	5b                   	pop    %ebx
f01018a9:	5e                   	pop    %esi
f01018aa:	5f                   	pop    %edi
f01018ab:	5d                   	pop    %ebp
f01018ac:	c3                   	ret    
f01018ad:	8d 76 00             	lea    0x0(%esi),%esi
f01018b0:	29 f9                	sub    %edi,%ecx
f01018b2:	19 d6                	sbb    %edx,%esi
f01018b4:	89 74 24 04          	mov    %esi,0x4(%esp)
f01018b8:	89 4c 24 08          	mov    %ecx,0x8(%esp)
f01018bc:	e9 18 ff ff ff       	jmp    f01017d9 <__umoddi3+0x69>
