在使用 `gdb` 进行调试的时候，应该打开到 jos 目录下面，因为在这个目录下面存在着一个 `.gdbinit` 文件，里面写好了与 gdb 有关的脚本，可以直接执行。如果没有执行，只需要在 gdb 中执行命令 `source ./.gdbinit` 就可以了。另外一种解决方法是设置安全路径，直接用命令 `gdb -q -iex 'add-auto-load-safe-path .' .gdbinit `即可。

